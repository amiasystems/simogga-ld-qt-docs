Fichier Excel
======================

.. include:: image.rst
.. include:: icon.rst

|quick_start_1|



SIMOGGA travaille avec un fichier xml.
Pour simplifier le remplissage de ce fichier, le fichier Excel est proposé: `AMIA-SIMOGGA-SampleCase.xlsm <http://www.amia-systems.com/uploads/8/3/6/7/8367487/1.amia-simogga-samplecase.xlsm>`_

Le fichier Excel avec les données pour la version Trial peut être téléchargé `ici <http://www.amia-systems.com/uploads/8/3/6/7/8367487/1.amia-simogga-trialcase.xlsm>`_

Ce fichier excel est composé de 5 onglets. Les trois premiers expliquent comment remplir les données nécessaire à SIMOGGA. L'onglet « Sample_Data » contient trois tableaux qui sont à compléter avec les données extraites de l'ERP. Le dernier onglet donne un visuel du process représenté dans l'onglet « Sample_Data ».

Comme expliqué dans la documentation de ce fichier Excel, les colonnes en jaune sont les seules obligatoires. Toutes les autres sont pour information ou pour aller plus loin dans l’analyse (par exemple avec l’analyse charge-capacité).

Tableau Produits
-----------------


|products|

Dans le tableau « Produits », chaque ligne correspond à une étape du process de production d’un produit. 
La séquence d’introduction de ces étapes pour chaque produit correspond à la séquence réelle. Les informations obligatoires sont pour chaque étape du process d’un produit : 

* le code produit

* la quantité à produire

* le code de la machine effectuant l’étape du process décrit

* la taille de lot de transfert pour aller de l’étape décrite à l’étape suivante (cela dépend en général du moyen de transport utilisé et du type d’emballage)

Tableau Machines
-----------------


|machines|

Le tableau « Machines » regroupe les informations liées aux machines, stations de travail, espaces de stockage. 
Les informations obligatoires sont :

* les codes machines utilisés par les produits 

* le type de la machine : chaque machine appartient à un ou plusieurs type machines en fonction du type d’opérations qui peut être effectué

Tableau Type Machines
----------------------------------


|machine_type|

Le tableau « Type Machine » reprend les différents types repris dans le tableau machine.

Conversion
-----------------


Une macro permet de convertir ce fichier Excel en fichier xml lisible par SIMOGGA.

Via le Menu de Excel : Outils/Macro/Macros, exécuter la macro « ConvertToXMLFile ».

Un fichier « SIMOGGA-output.xml » sera créé dans le répertoire commençant par XLS2XML (au même niveau que le fichier Excel).

