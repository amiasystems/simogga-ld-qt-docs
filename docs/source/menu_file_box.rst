Box des menus principaux 
========================

.. include:: image.rst
.. include:: icon.rst

Options/Paramètres
-------------------

Paramètres utilisateur
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|box_parameters|
 
* **Vue avec les IO** : Détermine si les matières premières/produits finis doivent être pris en compte dans le graphe de flux.

* **Vue avec les IO des cellules** : Pour la vue cellule, détermine si le graphe doit ou non passer par les points d’entrée/sortie.

* **Connecter le graphe** : Détermine si les machines doivent être connectées au graphe. Cette fonction est utile pour positionner l'ensemble des machines sur un plan d'usine très complexe, si le traitement est trop lourd.

* **Les matières MP/PF doivent être créés par SIMOGGA** : Détermine si les matières premières/produits finis doivent être créés lors de l’ouverture d’un cas. S'ils ont été ajoutés et que le fichier a été sauvé, SIMOGGA ne les crée plus. 

* **Visualisation du trafic** : Détermine le type de visualisation voulue pour afficher le trafic. Trois types sont disponibles :

	* **Standard** : Si les machines appartiennent à la même cellule, elles seront connectées entre elles en direct (flux intra-cellulaire). Dans le cas contraire, le trafic passera par les allées en suivant le plus court chemin.

	* **Par les allées** : Les machines seront automatiquement connectées entre elles par les allées. Il n'y aura aucune connexion intracellulaire entre les machines.
	
	* **A vol d'oiseau** : Les machines seront reliées entre elles par la plus courte distance à vol d'oiseau. La direction des flux est visible.

* **Flux proportionnel** : Détermine si l’épaisseur des flux affichés doit être proportionnelle. 

* **Vitesse de transport** : Détermine la vitesse de transport des produits au sein de l’usine.

* **Echelle** : Indique l’échelle du plan. Pour la modifier, il est nécessaire de passer par le mode design de la vue réelle. 

* **Coût par heure** : Détermine le coût par heure de transport au sein de l’usine.

* **Type de trafic** : Détermine le type de flux représenté. Le flux peut représenter :

	* Le nombre de transaction des produits : quantité de déplacements effectués.
	* Le nombre de produits transférés : quantité de pièces transférées.

* **Taille des machines** : Détermine la taille par défaut des machines.

* **Epaisseur maximum** : Détermine l’épaisseur maximale des flux.

* **Epaisseur minimm** : Détermine l’épaisseur minimale des flux.


ADMIN TAB
~~~~~~~~~~~~~~~~
 


Reroutage
----------------
 
Le menu de reroutage permet de réassigner une partie des opérations réalisés par une machine sur une autre machine du même type. 
Cela implique que la solution d'assignation des opérations sur les machines sera différente. Il faut donc créer un nouveau scénario pour ne pas travailler sur le scénario de base (option disponible via le clic droit sur le scénario, ou via le menu "Scénario").

|scenario_right_click|   

ou 

|menu_scenario_del_sce|

Les modifications réalisées au niveau du reroutage sont appliquées à toutes les alternatives du scénario courant. 
Pour rappel, les caractéristiques d'un scénario sont  :
* le nombre de machines présentes
* Une solution de routage (assignation des opérations sur les machines définissant le trafic/flux entre les machines) 
* un layout au niveau de la vue réelle

Les alternatives sont utilisées pour toute modification de la position des machines.

Quand on veut faire le reroutage sur le scénario de base, un message apparait : 

|message_reroutage_base_case|

Pour se faire, il est nécessaire de procéder de la manière suivante :

* Dupliquer le scénario de base

* Ouvrir l'outil de reroutage via le menu

|box_reroutage_open|

* Sélectionner la machine sur laquelle sont réalisés actuellement les opérations. 

|box_reroutage_selection_old|

* Sélection de la nouvelle machine sur laquelle doivent passer les flux (il s’agira en général d’une machine ayant été dupliquée). 

|box_reroutage_selection_new|

* Sélectionner la ou les machine(s) d'où proviennent les produits à réassigner (Origine).

|box_reroutage_selection_from|

* Sélectionner la ou les machine(s) vers lesquelles les produits à réassigner sont dirigés (Destination).

|box_reroutage_selection_to|

	* Ces deux machines sont dépendantes l’une de l’autre. 

	* Après avoir sélectionné l’une de ces deux machines, il est possible de sélectionner plusieurs machines From/To si les flux provenant ou allant vers la machine sélectionnés sont reliés à plus d’une machine From/To. 

	* Dans le cas où les MP(matières premières)/PF(produits finis) ne sont pas représentés, si la « old machine » est la première machine par laquelle passent les opérations, l’option IN (point d’entrée) sera la seule disponible dans la case From. Inversément, si la « old machine » est la dernière machine par laquelle passent les opération, l’option OUT(point de sortie) sera la seule disponible dans la case To.


* Si toutes les cases sont vertes, cliquer sur "OK" et les flux seront immédiatement réassignés

Remarques :

	* Il n’est pas possible de réassigner les flux pour le scénario de référence(Base). Pour effectuer une réassignation, il faudra au préalable dupliquer ce scénario. 

	* Si après sélection de la « old machine », aucune machine n’est présente dans la liste « new machine », cela signifie qu’il n’y a aucune autre machine de ce type disponible pour réassigner les flux. Dans ce cas, il sera nécessaire de dupliquer au préalable la « old machine ». 

	* Les machines listées en From et To sont dépendantes l’une de l’autre. Sélectionner une de ces deux machines entrainera une mise à jour de la liste opposée si aucune machine n’avait été sélectionnée dans celle-ci. Pour réinitialiser les deux listes, il faut cliquer sur la case blanche qui se trouve à son sommet.


Options/Filtres
------------------
.. pipe box_filter pipe

Produits
~~~~~~~~~~~~~~~~

|box_filter_product| 

Le menu des filtres par produit permet de n’afficher qu’une partie des flux du graphe. Il est possible d’effectuer un filtre :

* Sur base d’une sélection de produit : chaque produit est caractérisé par sa proportion de pièces et de transactions.

|box_filter_product_product|

* Sur base des familles de produits : au préalable, il est nécessaire de sélectionner l'alternative qui définit les familles à afficher. Ces familles ne seront proposées que si des cellules ont été définies dans l'alternative.

|box_filter_product_family|

* Sur base d'un pareto sur les produits ou les transactions : Permet de filtrer les produits sur base d’un pourcentage de pièces/transactions concernées.

|box_filter_product_pareto|

Informations affichées :

|box_filter_product_info|

* **Quantité de produits** : indique le nombre et la proportion de produits/pièces concernés par les produits filtrés.

* **Quantité de transactions** : Indique le nombre et la proportion de transactions concernées par les produits filtrés.

* **Nombre total de références produits** : Indique le nombre total de produits concernés par les produits filtrés. 



Machines
~~~~~~~~~~~~~~~~

|box_filter_machine| 

Le menu des filtres par machines permet de n’afficher qu’une partie des flux du graphe. Il est possible d’effectuer un filtre sur base d’une sélection de machines. Seules les machines sélectionnées seront reprises pour le graphe. 
Il est possible de (dé)sélectionner toutes les machines d’un coup en utilisant le bouton « select all ». 




Optimizer/Cell Optimizer
-------------------------------------------------

 

= Génération de solutions optimisées

 Quand on clique sur l'option "Cell Optimizer" du menu "Optimizer", une boite apparait pour sélectionner le scénario sur lequel le "Cell Optimizer" sera appliqué.

|box_optimizer_open|

Cell Optimizer permet la génération de manière automatique des solutions optimisées.
Pour ce faire, il faut au préalable sélectionner le scénario sur lequel on souhaite se baser pour la génération des solutions. 
 

Une fois cette sélection effectuée, un nouvel écran apparaitra où trois générations seront proposées. De base, seule la première, correspondant au mieux à la situation courante,  sera sélectionnée. Les deux autres propositions de génération seront des variantes de celle-là. 
 
|box_optimizercell|

Dans le cas où le scénario de Base a été sélectionné, il sera possible, pour chaque proposition, d’influer sur :

* Le nombre de duplication de chaque machine (Machines  x nbOccurences)

* Le nombre de cellules

* Le nombre maximum de machines par cellule

* Si oui ou non les MP(matières premières)/PF(produits finis) doivent se retrouver à l’intérieur des cellules.  (Attention : Les MP/PF ne sont pas pris en compte dans le nombre maximum de machines par cellule, il y aura par contre autant de couple MP/PF qu’il y a de cellules). 


Dans le cas où un autre scénario a été sélectionné, il sera possible, pour chaque proposition, d’influer sur :

* Le nombre de cellules

* Le nombre maximum de machines par cellules

* Si oui ou non les MP/PF doivent se retrouver à l’intérieur des cellules  (Le nombre de MP/PF ne sera pas dépendant du nombre de cellules). 

.. hint:: Dans ce second cas de figure, les machines prisent en compte dans la génération de la solution seront dépendantes des duplications effectuées par l’utilisateur pour le scénario sélectionnés. 
 
* Quand les solutions sont créées, elles peuvent être appliquées à l'un ou l'autre des scénarios.

|box_applysolution|

	* Sélectionner la solution optimisée à appliquer
	
	* Sélectionner le scénario qui appliquera la solution

	* Déterminer si le scénario doit être dupliqué

.. hint:: La solution en cellule est appliquée au niveau de la vue graphique. La solution de reroutage est appliquée à toutes les vues du scénario. Les machines ne seront pas changées de place dans la vue sur plan.