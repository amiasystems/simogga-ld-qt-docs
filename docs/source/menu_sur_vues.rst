Menu sur vues
====================

.. include:: image.rst
.. include:: icon.rst


Machines
-----------

Caractéristique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|machine|

* **Couleur** : la couleur défini le type de la machine.

* **Charge** : la remplissage coloré représente le pourcentage de charge par rapport à la capacité disponible.

* **Contour** : le contour de la machine permet de définir la surface au sol nécessaire à son placement.

Interactions machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Outre leurs déplacements à l'aide de |move|, d’autres interactions sont possibles avec les machines et, ce, quelle que soit la vue courante.

Options disponibles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* **clique droit** : 

	* **Dupliquer** : permet de dupliquer une machine. La machine dupliquée apparaitra alors à côté de celle-ci. Cette opération n'est pas possible sur le scénario de base. 

	|machine_rc_base|

	Pour pouvoir dupliquer une machine, il est au préalable nécessaire de dupliquer ce scénario. 

	|machine_duplicate|

..	* **Rotation** : permet de tourner la machine de 90 °.

..	pipe machine_rotate pipe

	* **Supprimer** : permet de supprimer la machine de l'alternative. 

	|machine_delete|

	Cette action définitive n'est possible que pour les machines dupliquées.

	|machine_rc_ref|
	
..	* **Afficher les IOs** : permet d'afficher l'entrée et la sortie de la machine. Cette option est intéressante quand la surface au sol est importante. 

..	pipe machine_io pipe
 
..	* **Bloquer la machine** : permet de bloquer la machine dans sa position sur le plan.

..	 pipe machine_locked pipe
 
* **Redimensionnement** : Quand la machine est sélectionnée, il est possible de redimensionner le contour définissant la surface au sol utile. Il faut déplacer à l'aide de la souris l'un des 8 points apparaissant autour d’elle. 

|machine_resized|

.. Flux
.. -----------

.. Cellules
.. -----------

.. Zones
.. -----------

.. Squelette d'allée
.. ----------------------
