Prise en main rapide
======================

.. include:: image.rst
.. include:: icon.rst

|quick_start|


1. Fichier Excel
-----------------


SIMOGGA travaille avec un fichier xml.
Pour simplifier le remplissage de ce fichier, le fichier Excel est proposé: `AMIA-SIMOGGA-SampleCase.xlsm <https://www.amia-systems.com/wp-content/uploads/2019/11/1.AMIA-SIMOGGA-XML-v2.7.3.xlsm.zip>`_

Le fichier Excel avec les données pour la version Trial peut être téléchargé `ici <https://www.amia-systems.com/wp-content/uploads/2019/11/1.AMIA-SIMOGGA-XML-v2.7.3.xlsm.zip>`_

Comme expliqué dans la documentation de ce fichier Excel, les colonnes en jaune sont les seules obligatoires. Toutes les autres sont pour information ou pour aller plus loin dans l’analyse (par exemple avec l’analyse charge-capacité).

La macro « ConvertToXMLFile » permet de convertir ce fichier Excel en fichier xml lisible par SIMOGGA.

Un fichier « SIMOGGA-output.xml » sera créé dans le répertoire commençant par XLS2XML (au même niveau que le fichier Excel).


2. Création de la situation AS-IS dans la **vue réelle**
-------------------------------------------------------------------------

La situation AS-IS est l'image de l'usine actuellement. Représenter cette situation actuelle va permettre de la quantifier et de l'utliser comme référence pour comparer tous les futurs scénarios.

Ouverture d'un fichier
~~~~~~~~~~~~~~~~~~~~~~~~~~

|open_file|

Boite de dialogue apparaît pour savoir si SIMOGGA doit créer les « machines » matière première (MP) et produit fini (PF). 
Si le jeux de données comprend déjà ces entrées et sorties du système, cliquer sur « Non ».

Pour la version Trial, cliquer sur  « Oui ». La représentation des flux est plus complète si l'on tient compte des entrées et sorties du système. 
 
|message_load_case|


Sauvegarde d’un cas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Sauvergarder : la sauvegarde se fera sur le fichier ouvert. Le fichier xml initial sera mis à jour. 

|menu_file_save|

* Sauvergarder sous… : L’utilisateur a la possibilité de choisir l’emplacement de la sauvegarde et le nom du fichier sauvegardé. 

|menu_file_save_as|


.. hint:: N'oubliez pas de sauvegarder votre travail régulièrement pour éviter toute perte lors de crash.



Création de l’usine – Mode Design
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans la vue réelle, il est possible d’obtenir une représentation en deux dimensions de la zone de travail correspondant à la réalité. 


* Dans le menu de navigation, cliquer sur scénario 1 (base) : S1RV1 - RealAlternative1

* Enregistrer cette alternative comme référence : clic droit sur l’alternative et sélectionner l’option « (Dé)sélectionner l’alternative de référence ». Cette situation servira de référence pour comparer tous les scénarios entre eux. 

|set_as_is_situation|

* Passer en « mode Design » 

|design_selection|

*  |insert_factory_plan| Insérer une image en arrière-plan correspondant au plan de l’usine. 

* Adapter la taille du plan à l’aide de la souris (Il faut que la taille de la machine de référence corresponde à la taille des machines sur le plan)

* |insert_factory_border| Dessiner le contour de l’usine sur l’image  

|process_real_2_factory|

* |insert_cell_border|  Déterminer les zones de travail (cellule) où pourront être placées les machines. 

|process_real_3_cell|

.. hint:: Les machines ne pourront être placées que sur ces zones cellules (jaune).   

* |scale|  Définir l’échelle du plan en cliquant sur deux points définissant une distance connue


Création du squelette d'allées
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le squelette est le graphe représentant les allées de l'usine par lequel va pouvoir passer le trafic entre les zones de travail (trafic intercellulaires). 

* |insert_skeleton_point| Insérer les points du squelette par double clique sur les intersections des allées.  

* Connexion automatique par SIMOGGA des points entre eux via des lignes si : 

	* la ligne ne traverse pas de zones 

	* les lignes ne passent pas trop près des coins de zones

	* les lignes ne sont pas trop proches

|process_real_4_skeleton|

* |insert_edge| Ajouter des lignes non créées automatiquement en double cliquant sur les deux points à connecter

* |remove_any_element| Supprimer des éléments sélectionnés (ligne, point...)

* |input| Ajouter des entrées et sorties (point IO) en bordure d’une zone. 
	
	|process_real_5_io|


* |insert_edge| Ajouter des allées dans les cellules

* Fixer une distance sur une ligne de squelette à l'aide du clic droit
		
	|process_real_7_setvalue|


Placement des machines – Mode Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Sélectionner le « mode Interaction »  dans la liste déroulante 

|interaction_selection|

* Déplacer les machines du BAC vers les zones cellules dans la position actuelle de l’usine (drag and drop)

* Choisir le type de visualisation :

	* |visualisation_standard|  standard : les machines sont connectées en direct si elles appartiennent à la même zone, sinon le trafic se fait par les allées en suivant le plus court chemin

	* |visualisation_byalleys| par les allées seulement : tout le trafic passent par les allées

	* |visualisation_crowfly| à vol d'oiseau : le graphe du squelette d'allées n'est pas utilisé. Les machines sont connectées entre elles via des flux directionnels


.. hint:: Dans les deux premières visualisations, la notion de direction disparait. C'est la totalité du trafic qui est représentée sur chaque segment. Ce mode de visualisation n'est possible que si un squelette d'allée a été construit dans le mode design. Dans le cas contraire, la représentation se fait à vol d'oiseau.



Les machines peuvent être modifiées par : 

* leur position via le "drag and drop"

* leur dimension pour représenter avec plus de précision la place réelle que prend la machine au sein de l’usine :

|process_real_9_machine_resize|

 
* |rotate_machine| leur orientation
	

* |lock_machine| leur immobilisation. Une machine cadenassée ne pourra plus bouger.
	
	|process_real_10_machine_lock|


Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Quand toutes les machines sont en position, les indicateurs de performance permettent d’évaluer la situation rapidement  

	* Nombre de kilomètres parcourus ;

	* Temps de parcours ;

	* Cout du transport.

|dashboard|


3. Analyse graphique des flux
------------------------------

|alternative|

Réorganisation
~~~~~~~~~~~~~~~~

Outils disponibles : 

* Les filtres par machine permettent d’analyser seulement les flux connectés à une ou plusieurs machines.

|filter_by_machine|
 
* Les filtres par produit permettent de visualiser les flux d’un ou plusieurs produits. 

|filter_by_product|

Dans l'écran de sélection, chaque produit est représenté par sa proportion de flux (en terme de quantité ou de mouvements) correspondant à l’ensemble de son process par rapport au nombre total de flux.

|filter_by_product_prod|

* Les filtres par famille de produit permettent de visualiser les flux relatifs à une ou plusieurs familles de produit quand on sélectionne au préalable une alternative. 

|filter_by_family|

* Un filtre Pareto peut être utilisé sur les quantités de produits transférés ou sur le nombre de transferts réalisés (Mouvements).  

|filter_pareto|

Ces filtres permettent de focaliser l’analyse sur une certaine partie des produits. 
Quelque soit le filtre utilisé, les informations concernant les produits filtrés sont disponibles en bas de la fenêtre. On y retrouve la quantité de pièces concernées ainsi que le nombre de transactions total. 

|filter_info|

Faire toute l’analyse de flux sur un Pareto des produits pourrait biaiser l’étude. Les outils disponibles dans SIMOGGA permettent de faire cette analyse pour la globalité des produits.

* Un filtre de visualisation permet de prendre en compte la totalité des produits mais de n’afficher que le flux entre une valeur minimum (Filtre bas) et une valeur maximum (Filtre haut).

|visualisation|


* Focaliser l'analyse sur une tranche de valeur avec une valeur minimale et maximale et regarder avec une loupe en cochant « Amplifier ». Les flux se différencieront les uns des autres. 

|extend_the_scale|


Reroutage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La représentation des flux donne une idée claire sur les machines liées entre elles mais pas sur le lien entre les machines du niveau N-1 et les machines du niveau N+1 par rapport à une machine de niveau N. (pour un flux allant de N-1 à N+1 en passant par N : N-1 -> N -> N+1). L’outil de décomposition des flux va permettre de différencier ces flux. 
L’idée de base de l’analyse qui suit est de trouver manuellement des groupes de machines indépendantes. Etant donné que tout ajout/suppression de machines ou réassignation des opérations sur une nouvelle machine implique de créer un nouveau scénario (voir prérequis), il faut commencer par créer un nouveau scénario vierge ou en dupliquer un existant.
Pour créer manuellement ces groupes indépendants, il faut dupliquer certaines machines et réallouer certaines opérations sur cette nouvelle machine. Pour ce faire, il faut utiliser l’outil « Reroutage »

|flow_decomposition|
 
Cet outil permet de suivre un flux (Ex : M5 –> M6 –> M4) et réassigner tous les flux de M6 sur M6_D1 (la machine dupliquée) pour l’ensemble des produit qui viennent de M5 et vont vers M4.
Il peut aussi être utilisé pour visualiser les flux connectés. L’écran nous montre que les produits qui passent de M6 vers M4 viennent soit de la machine M5 soit des produits matière première MP.
Cette analyse peut être longue en fonction du nombre de flux traités et du nombre de croisement de flux. Une astuce pour décomposer les flux est de procéder de manière systématique : 

* Identifier un flux principal que l’on veut isoler

* |insert_virtual_cell|  Définir une cellule virtuelle autour des machines que l'on veut isoler

* Commencer par dupliquer les MP (matière première). De cette manière, il ne faut regarder que deux niveaux de flux et pas trois. 

* Réassigner tous les flux venant de MP qui ne vont pas vers la branche principale identifiée

* Idem pour PF (produit fini)

* Identifier les machines que l’on veut dans le flux principal, isoler progressivement ces machines en les dupliquant et en réassignant tous les flux qui ne viennent pas ou ne vont pas vers la branche principale identifiée.

* Recommencer pour les autres branches



Création des cellules 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
Un algorithme d’optimisation est disponible dans SIMOGGA (Optimizer). 
Cet algorithme va chercher les groupements optimums des machines du scénario en assignant les opérations sur les machines les plus adéquates pour minimiser les flux entre les cellules. 

* Choisir le scénario de référence : les machines à grouper sont celles définies dans le scénario sélectionné.

|optimizer_open|

* Préciser le nombre de cellules voulues.

* Définir la taille des cellules par un nombre maximum de machines. 

* Définir si les matières premières (MP) et les produits finis (PF) doivent être dupliqués et insérés dans les cellules (pour une meilleure vision de l'indépendance des cellules)

* Sélectionner les cas à optimiser parmi les suggestions ("Générer")

|optimizer|

* Insérer de nouveaux cas à optimiser en sélectionnant à nouveau un scénario

.. ::hint Des suggestions sont faites en fonction du nombre de machines et du nombre de zones-cellules défini dans le scénario.

.. ::hint En travaillant avec le scénario de base, l’algorithme peut chercher la meilleure solution en dupliquant (1x, 2x) toutes les machines. Seul les machines utiles pour créer des cellules indépendant seront utilisées.

 
Evaluation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La création des cellules va permettre à SIMOGGA de générer des familles de produits assignées à ces cellules.  
Les produits sont alloués à une cellule en fonction du nombre de transactions réalisées au sein de la cellule (trafic intracellulaire). Ils sont présentés avec des couleurs différentes (du vert au rouge) en fonction de la pertinence de leur appartenance à la cellule.

Cette étape est intéressante pour connaître les flux directionnels entre les cellules que l’on voudrait créer dans la vue réelle. 
Les flux intracellulaire (interne aux cellules) sont évalués et présentés en terme de pourcentage des flux totaux.

|table_of_families|

4. Génération de scénarios
--------------------------

Sur base de la situation AS-IS et de l’analyse de flux précédente, il est possible de créer tous les scénarios possibles. 

Rappel : Un scénario possède un design d'usine unique, un nombre défini de machines et une matrice de flux (une solution de routage où les opérations sont chacunes assignées à une machines).

Une alternative sera utilisée pour tester différentes positions pour les machines.

Un scénario sera utilisé pour tester de  design (nouvelle zone de travail, allées déplacée...), des solutions avec de nouveaux investissements en terme de machines ou des nouvnouveaueaux routages pour les produits.



Par exemple, pour modifier les positions des machines de la situation As-Is

* Dupliquer « Alternative AS-IS »
* Changer les positions des machines. Il y a un impact direct sur les kilomètres parcourus (en vert si un gain est observé sinon en rouge)

|evaluation_positive|

|evaluation_negative|

* Analyser le dashboard

|use_scenarios|

Pour créer des solutions sur base de la réassignation des flux réalisée dans la vue graphique du « Scénario 2 » :

* Travailler sur le Scenario 2, alternative « S2RV1 » 

* Déterminer des changements incrémentaux (en terme de positionnement de machines) : 
	* Modifier « S2RV1 »
	* Dupliquer « S2RV1 » : Nouvelle alternative « S2RV2 »
	* Modifier « S2RV2 »
	* Dupliquer « S2RV2 » : Nouvelle alternative « S2RV3 »
	* Modifier « S2RV3 » 

..hint chaque état que l'on veut sauver doit être dupliqué pour continuer les modifications.

* Modifier le design de l'usine
	* Dupliquer le scénario 2  : Nouveau scénario « Scénario 3 » où seul la dernière solution du scénario 2 est conservé « S2RV3 »
	* Modifier le design (supprimer des cellules, créer de nouvelles cellules...)
	* Dupliquer « S3RV3 » : Nouvelle alternative « S3RV4 » (nouveau design avec les machines positionnées comme dans la dernière alternative du scénario 2)
	* Commencer à modifier l'alternative « S3RA3 » 
	* Procéder aux changements incrémentaux pour créer les différentes alternative « S3RV3 », « S3RV4 » en « S3RV5 »

Les modifications incrémentales (via les alternatives) peuvent aboutir à la libération d'une zone pour ensuite refaire le design de cette zone (via un nouveau scénario).


Fermeture
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|menu_file_close|

Lors de la fermeture d’un cas à l’aide la commande « Fermer » du menu « Fichier », il sera demandé à l’utilisateur s’il souhaite au préalable sauvegarder les modifications qu’il a effectuées.

|message_close|

S’il répond par l’affirmative, le cas actuellement traité sera sauvegardé dans le même fichier xml.  Dans le cas contraire, les modifications effectuées depuis le chargement du cas seront perdues. 





