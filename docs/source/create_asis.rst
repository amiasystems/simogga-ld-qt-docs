Création de la situation As-IS dans la **vue réelle**
======================================================

.. include:: image.rst
.. include:: icon.rst

|quick_start_2|



Ouverture d'un fichier
----------------------------------

L’ouverture du fichier « xml » commence sur la vue réelle. Les machines sont placées dans un bac. Elles ne pourront être placée sur la :term:`scène` que si des zones de travail (cellules jaunes) sont définies.
 
|open_file|

Lors de l’ouverture du fichier, une boite de dialogue apparaît pour savoir si SIMOGGA doit créer les « machines » matière première (MP) et produit fini (PF). 
Si le jeux de données comprend déjà ces entrées et sorties du système, cliquer sur « Non ».

Pour la version Trial, cliquer sur  « Oui ». La représentation des flux est plus complète si l'on tient compte des entrées et sorties du système. 
 
|message_load_case|

.. hint:: Si l’utilisateur charge un cas ayant déjà été sauvegardé alors qu’il contenait des MP/PF, ceux-ci apparaitront même si l’utilisateur choisit de ne pas les créer. En effet, ayant déjà été créées au préalable, elles font désormais partie des machines traitées dans le cas présent. 
 
Si SIMOGGA ne parvient pas à charger le fichier « xml » sélectionné, une fenêtre d’erreur apparaitra pour le signifier à l’utilisateur. Pour que SIMOGGA soit capable de charger un fichier « xml », la structure de celui-ci doit correspondre à la structure mise en place par SIMOGGA. 

|message_dialog_openfail|

Vérifier que le fichier « xml » ne contient pas de caractères spéciaux tels que : 

#DIV/0!  -  #N/A  -  #VALEUR!  -  /  -  \*  -  '  -  (  -  )  -  &

Sauvegarde d’un cas
----------------------------------

Deux types de sauvegardes sont possibles et se retrouve dans le menu « Fichier » :

* Sauvergarder : la sauvegarde se fera sur le fichier ouvert. Le fichier xml initial sera mis à jour. 

|menu_file_save|

* Sauvergarder sous… : L’utilisateur a la possibilité de choisir l’emplacement de la sauvegarde et le nom du fichier sauvegardé. 

|menu_file_save_as|


.. hint:: N'oubliez pas de sauvegarder votre travail régulièrement pour éviter toute perte lors de crash.



Création de l’usine – Mode Design
----------------------------------

Dans la vue réelle, il est possible d’obtenir une représentation en deux dimensions de la zone de travail correspondant à la réalité. 


* Cliquer sur le scénario 1 (base)

|scenario|

* Dans « alternative réelle », cliquer sur S1RV1 - RealAlternative1

* Enregistrer cette alternative comme référence : clic droit sur l’alternative et sélectionner l’option « (Dé)sélectionner l’alternative de référence ». Cette situation servira de référence pour comparer tous les scénarios entre eux. 

|set_as_is_situation|

ou via le menu du scénario : 

|set_as_is_situation_by_scenario|

* Sélectionner le « mode Design »  dans la liste déroulante.  

|design_selection|

* Le bac machine n'est plus visible. Une machine de référence apparait dans le coin en haut à gauche. Cette machine représente la machine de taille minimum voulue par l'utilisateur.

|insert_factory_plan| Insérer l'image de l'usine en arrière-plan
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Inserer le plan

	* Cliquer sur l'icône |insert_factory_plan| 

	* Une fenêtre s'ouvre

	* Sélectionner un plan d’usine au format png ou jpg

	* Cliquer sur Ok

* Adapter le plan 

	* Déplacer le plan si besoin à l’aide de la souris

	* Cliquer sur le plan pour l’activer

	* Modifier la taille du plan via le zoom avec la roulette de la souris

	* Il faut que la taille de la machine de référence corresponde à la taille des machines sur le plan

	.. ::hint C'est bien le plan que l'on adapte à la machine de référence. La taille de cette machine ne peut pas être modifiée

	* Pour sortir de cette adaptation, appuyer sur la touche « esc » du clavier ou cliquer sur le bouton |move|


|insert_factory_border| Dessiner le contour de l’usine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	* Cliquer sur l'icône |insert_factory_border| 

	* Créer le contour par de simples clics successifs sur les coins du plan. Chaque clic de souris correspond à un point du polygone représentant ce contour

	* Terminer par un double clic (sur l'avant dernier point) pour indiquer que le polygone est complet et il se fermera automatiquement

	* Choisir le type de zone voulue (site, bâtiment, étage, aire)

	* Renommer la zone

|process_real_2_factory|


|insert_cell_border|  Déterminer les zones de travail (cellule) 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les machines ne pourront être placées que dans ces zones pour être connectées. 

	* Sélectionner la zone « parent » à laquelle appartiendra la cellule en cliquant dans le menu de navigation ou sur la zone usine. Quand la zone est active, elle est verte.

	|panel_lateral_structure_select_site|
	
	* Cliquer sur l'icone |insert_cell_border|
	
	* Si la zone parent n'est pas sélectionnée, un message apparait. 

	|message_parent_selection|

	.. hint:: Il faut alors sélectionner le parent dans le menu latéral de navigation. La scène n'est pas accessible tant que le bouton |insert_cell_border| est enfoncé.
	.. hint:: Pour désactiver n’importe quelle fonctionnalité, appuyer sur « esc ».

	* Créer un polygone à l’aide d’une succession de clic de souris. Cliquer sur les coins qui vont définir la cellule.
	
	* Le contour ne doit pas passer juste sur le bord de l'usine. La zone sera automatiquement coupée pour être alignée sur le bord de l'usine.

	* Terminer par un double clic (sur l'avant dernier point) pour indiquer que le polygone est complet et il se fermera automatiquement. 

|process_real_3_cell|


.. hint:: Les machines ne pourront être placées que sur ces zones cellules (jaune).   

.. hint:: Si on veut dessiner plusieurs cellules, maintenir la touche « Ctrl » (sous WIN) ou « Cmd » (sous MAC) enfoncée.



|scale|  Définir l’échelle du plan 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	* Cliquer sur le bouton  |scale|

	* Utiliser une distance connue sur le plan

	* Cliquer (simple clic) sur les deux points définissant cette distance

	|set_scale|

	* Définir la distance en mètre

	* Cliquer sur Ok



Création du squelette d'allées
----------------------------------

Le squelette est le graphe représentant les allées de l'usine par lequel vont pouvoir passer le trafic entre les zones de travail (trafic intercellulaires). Cet outil est disponible dans le mode design de l'alternative réelle.

* Sélectionner la zone où l'on veut mettre des allées via la scène ou le menu de navigation

* Cliquer sur l'icone  |insert_skeleton_point| 
	
* Insérer les points du squelette par double clique sur les intersections des allées. Ces points représentent les points de connexion des allées.  

* Connexion automatique par SIMOGGA des points entre eux via des lignes si

	* la ligne ne traverse pas de zones 

	* la ligne ne passe pas trop près des coins des zones

	* les lignes ne sont pas trop proches

|process_real_4_skeleton|

* Ajouter des lignes non créées automatiquement

	* Activer le bouton  |insert_edge|

	* Double clic sur les deux points à connecter


* Supprimer des lignes non voulues

	* Sélectionner l'élément à supprimer

	* Sélectionner plusieurs éléments en maintenant la touche Ctrl/Cmd enfoncée

	* Cliquer sur le bouton |remove_any_element|
 
	* Cliquer sur Ok

* Ajouter des entrées et sorties (point IO) en bordure d’une zone. 
	
	|process_real_5_io|

	* Activer le bouton  |input|

	* Double cliquer si la bordure de la zone

	* Clic droit sur le point ajouté pour modifier son type.
		* IN : Seuls les flux entrants pourront passer. 
		* OUT : Seuls les flux sortants pourront passer.
		* I/O : Tous les flux pourront passer. 


.. hint:: Les points IO seront utiles dans le cas où on se trouve face à une usine comportant plusieurs étages et/ou bâtiments, ou si l’on souhaite travailler sur plusieurs sites simultanément. 
 
* Ajouter des allées dans les cellules.

	* Activer le bouton  |insert_edge|

	* Double-clique sur le IO d'une cellule, puis double clic sur le point suivant. 

		- Si un point existe, il sera connecté au IO
		- Sinon, un nouveau point sera créé. 

	* Recommencer jusqu'à la création complète des allées dans la cellule.


* Fixer une distance sur une ligne de squelette. 
		
	.. hint:: Ce sera particulièrement utile lorsque l’on souhaite travailler avec plusieurs sites simultanément. Une simple ligne de squelette peut représenter un nombre défini de mètres ou kilomètres. 

	|process_real_7_setvalue|

	* Clic droit sur la ligne à fixer 

	* Sélectionner l'option « Fixer la distance»
	
	* Rentrer une valeur en mètres ou kilomètres

	* Clic droit sur la ligne pour afficher la valeur entrée

	* Supprimer la valeur via l'option « Réinitialiser...»

Validation du design
------------------------------------------------------------

Chaque point du squelette apparait avec un bord rouge. 

|skeletonpoint_connected|

Si le point est rempli en rouge, cela signifie qu'il n'est pas connecté au graphe d'allée.

|skeletonpoint_notconnected|


Pour vérifier les connexions, cliquer sur le bouton |nodes|.

Chaque point du squelette possède une caractéristique graphique qui permet de valider les connexions. Les petites lignes bleues au départ du point montre que le point est bien connecté à la ligne.

|skeletonpoint_valide|

En passant en mode interaction, un message apparaît si le graphe présente des défauts de connexion comme un point du squelette isolé.

Dans le panneau latéral d'information, les machines utilisées (charge > 0) seront listées en rouge si elles ne sont pas connectées au graphe.

|panel_lateral_machine_notconnected|


Placement des machines – Mode Interaction
------------------------------------------------------------

Positionnement
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand des zones de travail (en jaune) ont été créée dans le mode design, les machines peuvent y être placées. Pour ce faire, un simple « drag & drop » suffit. 

* Sélectionner le « mode Interaction »  dans la liste déroulante 

|interaction_selection|

* Déplacer les machines du BAC vers les zones cellules dans la position actuelle de l’usine 

	* En cliquant sur CTRL/CMD on peut sélectionner plusieurs machines

	* Les machines seront automatiquement connectées au graphe d’allées. 

	* Si il existe un flux entre deux machines positionnées sur le plan, il apparaitra automatiquement.


Visualisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Choisir le type de visualisation :

	* |visualisation_standard|  standard : les machines sont connectées en direct si elles appartiennent à la même zone, sinon le trafic se fait par les allées

	* |visualisation_byalleys| par les allées seulement : toute le trafic passent par les allées

	* |visualisation_crowfly| à vol d'oiseau : le graphe du squelette d'allées n'est pas utilisé. Les machines sont connectées entre elle via des flux directionnels



.. hint:: Dans les deux premières visualisations, la notion de direction disparait. C'est la totalité du trafic qui est représenté sur chaque segment. Ce mode de visualisation n'est possible que si un squelette d'allée a été construit dans le mode design. Dans le cas contraire, la représentation se fait à vol d'oiseau.


Interaction des machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les machines peuvent être modifiées par : 

* leur position via le « drag and drop » 

* leur dimension pour représenter avec plus de précision la place réelle que prend la machine au sein de l’usine :

|process_real_9_machine_resize|

	* Sélectionner la machine

	* Affichage des carrés sur le contour

	* Déplacer l'un des carrés du contour
 
* leur orientation :
	
	* Cliquer sur le bouton « Rotation » rotate_machine|

	* Double clic sur la machine

	* Ou fonction disponible via le clic droit sur la machine

* leur immobilisation. Une machine cadenassée ne pourra plus bouger :
	
	|process_real_10_machine_lock|

	* Cliquer sur le bouton |lock_machine|

	* Double clic sur la machine

	* Affichage du cadenas sur la machine

	

KPIs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Quand toutes les machines sont en position, les indicateurs de performance permettent d’évaluer la situation rapidement  

	* Nombre de kilomètres parcourus ;

	* Temps de parcours ;

	* Cout du transport.

|dashboard|




