Menu latéraux
==================
.. include:: image.rst
.. include:: icon.rst


|panel_lateral|

Panneau de navigation scénarios/alternatives
----------------------------------------------

|panel_lateral_scenario|

Le panneau de navigation des scénarios permet d'avoir une vue d'ensemble des vues graphique, cellule et réelle (sur plan) qui ont été créées.
Par défaut, à l'ouverture, le scénario est créé avec une vue de chaque type.
Chaque alternative est définie par une code (S1RV1) qui reprend 

* le numéro du scénario => Ex : S1

* le type de l'alternative GV (graphique), CV (cellule), RV (réelle/sur plan) => Ex : RV

* le numéro de l'alternative => Ex : 1

A côté de ce code, le nom de l'alternative peut être modifié en faisant un double clique.

Par ce panneau et via les clique droit, il est possible d'accéder à différents menus au niveau du scénario et de l'alternative.

* Clic droit sur le scénario : 

|scenario_right_click|

* Clic droit sur l'alternative graphique

|alternative_right_click|

* Clic droit sur l'alternative réelle

|set_as_is_situation|



.. Panneau de navigation fournisseurs
.. ----------------------------------------------
.. pipe panel_lateral_supplier pipe

Panneau de design
----------------------------------------------

|panel_lateral_structure|

Le panneau de navigation permet de sélectionner les différents élements du design réalisé dans la vue réelle du scénario. 
C'est dans ce panneau que l'on peut activer une zone, pour y dessiner les cellules ou ajouter des points du squelette d'allée.
La zone active est mise en bleu.
Chaque zone possède un "parent", c'est la zone au-dessus qui le contient.
Dans l'exemple, le site Site2 est parent des cellules Cell3 à Cell6. Ces 4 cellules sont les enfants de Site2.

Panneau d’informations
----------------------------------------------

|panel_lateral_info|

Ce panneau permet d'afficher les informations quand on clique sur un élement.

Machine
~~~~~~~~~~~~~~~~
* Nom tronqué : nom affiché sur la machine
* Nom : nom complet de la machine
* ID : identifiant interne à SIMOGGA
* Machine de référence : si la machine a été dupliquée, la machine de référence est la machine initiale
* Type principal : définit le type de la machine
* Charge : définit la charge de la machine
* Capacité : définit la capacité de la machine
* % : défini le taux de remplissage de la machine. Le remplissage coloré de la machine correspond à cette valeur.

Flux
~~~~~~~~~~~~~~~~
* Source : Nom de la machine à l'origine du trafic
* To : Nom de la machine de destination du trafic
* Nb références : nombre de référence produit passant entre ces deux machines
* Nb pièces : nombre de produits transférés, toutes référénces confondues.
* Nb transactions : nombre de transferts effectués de la machine Source vers la machine destination




Tableau de bord
----------------------------------------------

|panel_lateral_dashboard|

Le tableau d'évaluation est basé sur un comparatif des alternatives réelles avec l'alternative qui a été sélectionnée comme référence. 

|menu_scenario_as_is|

* Colonne 1 (Alt) : code de l'alternative
* Colonne 2 (Coût) : coût en euro des kilomètre parcourus (basé sur le cout horaire défini dans le panneau de configuration)
* Colonne 3 (%) : % de gain par rapport à la situation de référence
* Colonne 4 (Nb kms) : définit le nombre total de kilomètre parcourus par l'ensemble des produits
* Colonne 5 (Temps) : définit le temps total minimum dédié au transport des produits


Menus en bas
----------------------------------------------

Filtres
~~~~~~~~~~~~~~~~

Ce filtre de visualisation permet de prendre en compte la totalité des produits mais d’afficher seulement le flux entre une valeur minimum (Filtre bas) et une valeur maximum (Filtre haut).

|panel_filter|

|panel_filter_0-100_view| 

Ce filtre est utilisé pour réorganiser le diagramme spaghetti présenté dans la vue graphique. En plaçant le filtre bas (le bouton en bas du filtre) vers les grandes valeurs (par exemple, 60), seul les flux dont la valeur est inclue entre 60% et 100% du trafic maximum seront affichés. Les machines liées par ces flux peuvent être réparties sur la droite de l'écran.

|panel_filter_60-100|

|panel_filter_60-100_view|

En déplacant progressivement ce filtre bas vers la gauche, on prendra en compte de plus en plus de flux pour réorganiser les machines.

|panel_filter_25-100|

|panel_filter_25-100_view|

Si il existe beaucoup de flux dans une tranche de valeur (par exemple entre 0 et 25% du flux maximum), il faut encadrer ces valeurs en placant le filtre bas sur 0 et le filtre haut sur 25. 

|panel_filter_0-25|

|panel_filter_0-25_view|

On peut alors amplifier la vue pour distinguer chacun de ces flux qui peuvent paraitre identique à première vue et rendre la réorganisation difficile.

|panel_filter_0-25_magn|

|panel_filter_0-25_view_magnify| 

Epaisseur des flux
~~~~~~~~~~~~~~~~~~~~

|panel_thickness|

Deux sliders permettent de fixer l'épaisseur minimum et maximum des flèches qui représentent le trafic.
