.. file with all image icons to include
 
.. design
.. |insert_factory_plan| image:: _static/img/icons/insert_background_fr.png
.. |insert_factory_border| image:: _static/img/icons/insert_factory_border_fr.png
.. |insert_cell_border| image:: _static/img/icons/insert_cell_border_fr.png
.. |insert_skeleton_point| image:: _static/img/icons/design_fr.png
.. |insert_edge| image:: _static/img/icons/line_fr.png
.. |insert_io| image:: _static/img/icons/output_fr.png
.. |scale| image:: _static/img/icons/scale_fr.png
.. |nodes| image:: _static/img/icons/nodes_fr.png
.. |connect| image:: _static/img/icons/connect_fr.png

.. |remove_any_element| image:: _static/img/icons/delete_fr.png

.. machine
.. |lock_machine| image:: _static/img/icons/lock_fr.png
.. |rotate_machine| image:: _static/img/icons/rotate_fr.png
.. |multiple_selection| image:: _static/img/icons/selectMultiple_fr.png
.. |delete_item| image:: _static/img/icons/delete_fr.png

.. cell
.. |insert_virtual_cell| image:: _static/img/icons/virtualCell_fr.png
.. |removeMachineFromVirtualCell| image:: _static/img/icons/removeMachineFromVirtualCell_fr.png
.. |table_of_families| image:: _static/img/icons/table_fr.png
.. |input| image:: _static/img/icons/output_fr.png
.. |output| image:: _static/img/icons/output_fr.png

.. general view
.. |move| image:: _static/img/icons/select_fr.png
.. |hide_background| image:: _static/img/icons/hide_background_fr.png
.. |panel_machine| image:: _static/img/icons/store_fr.png
.. |empty_machine| image:: _static/img/icons/machine_fr.png
.. |connect_machine| image:: _static/img/icons/connect_fr.png
.. |prevent_collision| image:: _static/img/icons/collision_fr.png

.. |visualisation_type| image:: _static/img/icons/type_visualization_fr.png
.. |visualisation_byalleys| image:: _static/img/icons/byalleys_fr.png
.. |visualisation_crowfly| image:: _static/img/icons/crowfly_fr.png
.. |visualisation_standard| image:: _static/img/icons/standard_fr.png

