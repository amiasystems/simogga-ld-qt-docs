Génération de scénarios
======================================================

.. include:: image.rst
.. include:: icon.rst

|quick_start_4|


Comment utiliser les scénarios et alternatives ?
----------------------------------------------------

Sur base de la situation AS-IS et de l’analyse de flux précédente, il est possible de créer tous les scénarios possibles. 

.. ::hint Un scénario possède un design d'usine unique, un nombre défini de machines et une matrice de flux (une solution de routage où les opérations sont chacunes assignées à une machines).

Une alternative est utilisée pour tester différentes positions pour les machines.

Un scénario est utilisé pour tester de nouveau design (nouvelle zone de travail, allées déplacée...), des solutions avec de nouveaux investissement en terme de machines ou des nouveaux routages pour les produits.


Modifier les positions des machines de la situation As-Is
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Dupliquer « Alternative AS-IS »
* Changer les positions des machines. Il y a un impact direct sur les kilomètres parcourus (en vert si un gain est observé sinon en rouge)

|evaluation_positive|

|evaluation_negative|

Nouveau scénario sur base de la réassignation des flux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour créer des solutions sur base de la réassignation des flux réalisée dans la vue graphique du « Scénario 2 »

* Travailler sur le Scenario 2, alternative « S2RV1 » 

* Déterminer des changements incrémentaux (en terme de positionnement de machines) : 
	* Modifier « S2RV1 »
	* Dupliquer « S2RV1 » : Nouvelle alternative « S2RV2 »
	* Modifier « S2RV2 »
	* Dupliquer « S2RV2 » : Nouvelle alternative « S2RV3 »
	* Modifier « S2RV3 » 

..hint chaque état que l'on veut sauver doit être dupliqué pour continuer les modifications.

* Modifier le design de l'usine
	* Dupliquer le scénario 2  : Nouveau scénario « Scénario 3 » où seul la dernière solution du scénario 2 est conservé « S2RV3 »
	* Modifier le design (supprimer des cellules, créer de nouvelles cellules...)
	* Dupliquer « S3RV3 » : Nouvelle alternative « S3RV4 » (nouveau design avec les machines positionnées comme dans la dernière alternative du scénario 2)
	* Commencer à modifier l'alternative « S3RA3 » 
	* Procéder au changements incrémentaux pour créer les différentes alternative « S3RV3 », « S3RV4 » en « S3RV5 »

Les modifications incrémentales (via les alternatives) peuvent aboutir à la libération d'une zone pour ensuite refaire le design de cette zone (via un nouveau scénario).

Utilisation du Dashboard
----------------------------------------------------

|process_real_11_dashboard|

Le dashboard permet de comparer différentes alternatives réelles afin de visualiser rapidement et simplement quelle est la meilleure d’entre elles. Il contient plusieurs données :

* Le nom de l’alternative et son scénario

* Le cout total que représente la situation

* Le pourcentage de gain de cette situation par rapport à celle indiquée comme la référence

* Le nombre total de kilomètres que parcourent les produits

* Le temps total écoulé

Pour déterminer quelle est l’alternative de référence, il faut soit passer par le menu Scénario, soit effectuer un clic droit sur l’alternative sélectionnée et choisir l’option « set as baseline ». Le calcul du pourcentage de gains des autres alternatives se fera alors sur base de cette alternative de référence. 


Fermeture
------------------------------

|menu_file_close|

Lors de la fermeture d’un cas à l’aide la commande « Fermer » du menu « Fichier », il sera demandé à l’utilisateur s’il souhaite au préalable sauvegarder les modifications qu’il a effectuées.

|message_close|

S’il répond par l’affirmative, le cas actuellement traité sera sauvegardé dans le même fichier xml.  Dans le cas contraire, les modifications effectuées depuis le chargement du cas seront perdues. 
