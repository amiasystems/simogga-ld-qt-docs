```html
  ____    ___   __  __    ___     ____    ____      _              _       ____             ___    _             ____                           _____   _   _ 
 / ___|  |_ _| |  \/  |  / _ \   / ___|  / ___|    / \            | |     |  _ \           / _ \  | |_          |  _ \    ___     ___   ___    | ____| | \ | |
 \___ \   | |  | |\/| | | | | | | |  _  | |  _    / _ \    _____  | |     | | | |  _____  | | | | | __|  _____  | | | |  / _ \   / __| / __|   |  _|   |  \| |
  ___) |  | |  | |  | | | |_| | | |_| | | |_| |  / ___ \  |_____| | |___  | |_| | |_____| | |_| | | |_  |_____| | |_| | | (_) | | (__  \__ \   | |___  | |\  |
 |____/  |___| |_|  |_|  \___/   \____|  \____| /_/   \_\         |_____| |____/           \__\_\  \__|         |____/   \___/   \___| |___/   |_____| |_| \_|
                                                                                                                                                                                                                                                                                                                                                                             

```
## Documentation: SIMOGGA optimises your factory layout

SIMOGGA™ is a graphical material-handling system allowing engineers to optimise a factory layout based on a global flow analysis using few amount of data.

The flow analysis uses the product information (batch size, setup time, process) and the machine features (capacity, capability) to generate machine repartition and evaluate material flow distances and the charge on each machine.

SIMOGGA™ optimises functional, job-shop and process layouts by converting them into working cells or departments. 

AMIA Systems accompanies during Merging & Acquisitions or Joint Ventures and help evaluate purchase and sale proposals while participating to the due diligence investigations.

The [documentation of SIMOGGA] is available on our website

#### Learn about SIMOGGA on [AMIA Systems]
#### Find us on the social networks 
##### [AMIA Systems Facebook]
##### [AMIA Systems Twitter]

[AMIA Systems Facebook]: https://www.facebook.com/AmiaSystems
[AMIA Systems Twitter]: https://www.twitter.com/amiasystems
[AMIA Systems]: http://www.AMIA-Systems.com
[documentation of SIMOGGA]: http://simogga-ld-qt-docs.rtfd.org

DOCUMENTATION
===============

* http://fr.wikipedia.org/wiki/ReStructuredText
* http://docutils.sourceforge.net/rst.html

git documentation
------------------


1. Dans terminal
1. `cd ~/SIMOGGA/source_code/bitbucket.org/amiasystems/simogga-ld-qt-docs`
1. Work and Save
    1. git status : ce qu’on a fait
    1. `git add -A`
    1. `git commit -m ’txt ‘`
    1. `git push`

sphinx documentation
--------------------

1. Dans terminal
1. `cd ~/SIMOGGA/source_code/bitbucket.org/amiasystems/simogga-ld-qt-docs/docs`
    1. `rm -r build/ && make html && open build/html/index.html`

`curl -X POST http://readthedocs.org/build/simogga-ld-qt`


[Text ASCII Art Generator]: http://patorjk.com/software/taag/#p=display&h=0&v=0&f=Ivrit&t=SIMOGGA-LD-Qt-Docs%20EN


Generate documentation files for qt
--------------------

1. Dans terminal
1. `cd ~/SIMOGGA/source_code/bitbucket.org/amiasystems/simogga-ld-qt-docs/docs`
1. `rm -r build/ && make qthelp && cd build/qthelp/`
1. `qhelpgenerator SIMOGGALDQt.qhp -o SIMOGGALDQt.qch`
1. `qcollectiongenerator SIMOGGALDQt.qhcp -o SIMOGGALDQtCollection.qhc`
