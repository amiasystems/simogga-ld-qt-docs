.. SIMOGGA LD Qt documentation master file, created by
   sphinx-quickstart on Tue Aug 26 13:57:17 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenu sur la documention de SIMOGGA LD Qt
============================================

SIMOGGA, un outil innovant qui vous donne une vue globale du trafic et de l’agencement (layout) de votre usine.

Cet outil se place en amont d’un projet d’optimisation industrielle. Avant de se lancer dans l’amélioration localisée d’une ligne de production, il est indispensable d’avoir une vue globale et macro des flux pour prendre conscience des trajets réels empruntés par les produits dans l’usine. 

SIMOGGA est constitué d’un cœur d’optimisation issu de 10 ans de recherche et d’une interface de visualisation interactive et intelligente. 
Cette interface se veut facile d’utilisation et peu gourmande en données. Elle oriente l’utilisateur dans sa démarche d’amélioration mais ne le remplace pas. Elle se nourrit du savoir et de l’expérience (du terrain) récoltée tant auprès des « plant managers » que des opérateurs.

Bon travail 

.. note:: N'oubliez pas que vous pouvez nous contacter à tout moment via cette page: http://www.amia-systems.com/contact-us.html



Contenu :

.. toctree::
   :maxdepth: 2
   
   prerequisite
   prise_en_main_rapide
   excel_file
   create_asis
   flow_analysis
   layout_generation
   datapanel
   menu_principal
   menu_vues
   menu_sur_vues
   menu_lateral
   menu_file_box
   traitement_cas_complet
   glossary
.. prise_en_main_rapide_en
   
..   menu_vues_en
..   menu_sur_vues_en
..   menu_lateral_en
..   menu_file_box_en
..   traitement_cas_complet_en


Index et tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`