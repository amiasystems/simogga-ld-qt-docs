Menu vues
==============

.. include:: image.rst
.. include:: icon.rst

Vue graphique
--------------

|panel_graphic|


* |multiple_selection|  **Multi-selection** : Permet de sélectionner plusieurs machines simultanément à l’aide d’un polygone de sélection dessiné par l’utilisateur (simple clic autour des machines et double clic pour fermer le contour).


* |delete_item|  **Supprimer un objet** : Permet de supprimer un élément. Au sein de la vue graphique, il est uniquement possible de supprimer une cellule virtuelle.


* |empty_machine|  **Cacher les machine vides** : Permet de cacher ou d’afficher les machines dont l’utilisation est nulle.


* |connect|  **Connecter/Déconnecter les machines** : Permet de cacher ou d’afficher les flux passant entre les machines.


* |prevent_collision|  **Interdit la superposition** : Permet d’éviter à deux machines de se superposer.


* |removeMachineFromVirtualCell|  **Libérer les machines** : Permet aux machines de ne plus être bloquées à l’intérieur de la cellule dans laquelle elles se trouvent


* |input|  **Ajouter des points d'entrée et sortie IN/OUT** : Permet d’ajouter un point d’entrée/sortie à une cellule virtuelle en double-cliquant sur l’une des bordures de cette cellule.


* |lock_machine|  **Bloquer une machine** : Permet de bloquer une machine. Cette machine ne pourra dès lors plus être déplacée.


* |insert_virtual_cell|  **Définir une cellule virtuelle autour des machines** : Permet d’ajouter une cellule virtuelle qui prend la forme d’un rectangle extensible. Ces cellules virtuelles permettront de définir des familles de produits.


* |table_of_families|  **Montrer le tableau des familles de produits par cellule** : Permet d’afficher le tableau reprenant, pour chaque cellule virtuelle, les produits qui lui sont associés.


.. * |rotate_machine|   **Rotate machine** : Permet d’effectuer une rotation de la machine.



Vue sur plan - Mode interaction
------------------------------------------

|panel_real_interaction| 

* |delete_item|  **Supprimer un objet** : Permet de supprimer un élément. Au sein de la vue réelle en mode interaction, il est uniquement possible de supprimer les points d’entrées/sorties.


* |hide_background|  **Cacher le plan** : Permet de cacher ou d’afficher l’image de fond ayant été sélectionnée.

* |panel_machine|  **Cacher le  panneau des machines** : Permet de cacher ou d’afficher le bac contenant les machines ne se trouvant actuellement pas au sein de l’usine.

* |empty_machine|  **Cacher les machines vides** : Permet de cacher ou d’afficher les machines dont l’utilisation est nulle.

* |connect_machine|  **Connecter/Déconnecter les machines** : Permet de positionner les machines sur le plan sans qu’elles se connectent. Option intéressante quand le rafraichissement est trop lent. A la fin du placement de toutes les machines, repasser en position « Connect » pour mettre la situation à jour.

* |prevent_collision|  **Eviter les superpositions de machines** : Permet d’éviter que deux machines se superposent

* |lock_machine|  **Bloquer la machine** : Permet de bloquer une machine. Cette machine ne pourra dès lors plus être déplacée.

* |rotate_machine|  **Tourner une machine** : Permet d’effectuer une rotation de la machine.

.. * |multiple_selection|  **Multi-selection** : Permet de sélectionner plusieurs machines simultanément à l’aide d’un cadre de sélection dessiné par l’utilisateur.

*   |visualisation_standard|  **Visualisation** : Permet de choisir entre différents types de visualisation des flux

|visualisation_type|

	* |visualisation_standard| **Visualisation standard** : permet de connecter les machines qui appartiennent à la même cellule en direct si aucune allée n'est définie dans la cellule, sinon le trafic passe par les allées par plus court chemin.

	* |visualisation_byalleys| **Visualisation par les allées** : aucune connexion directe ne se fait entre les machines. Tout le trafic passe par les allées.

	* |visualisation_crowfly| **Visualisation via des distances à vol d'oiseau** : les machines sont connectées entre elles en direct sans passer par les allées. Cette vue permet de visualiser la direction des flux.



Vue sur plan - Mode design
------------------------------------------

|panel_real_design| 

* |move|  **Sélectionner et déplacer un élement** : Permet de déplacer un élément.


* |delete_item|  **Supprimer un objet** : Permet la suppression d’un objet ou plusieurs objets sélectionnés (grâce à la touche Ctrl/Cmd). 


* |insert_factory_plan|  **Ajouter un plan en fond d'écran** : Permet l’ajout d’une image de fond pour calquer le plan de l’usine.


* |scale|  **Définir l'échelle** : Permet de modifier l’échelle de l’usine. L’utilisateur a la possibilité de dessiner une ligne (un clic sur chaque extrémité) et de donner une valeur en mètre à celle-ci


* |insert_factory_border|  **Créer une zone** : Permet la création d’une zone (Site, Building, Floor ou Area) représentée par un polygone que l’utilisateur aura la possibilité de dessiner. Cliquer sur le plan pour positionner les points du contour. Pour fermer la zone, faire un double clic sur le dernier point du contour. Il n’est pas utile de double cliquer sur le premier point. Si la zone se situe à l’intérieur d’une zone parent (premier point dans une zone parent), elle sera automatiquement tronquée pour que ses limites correspondent à cette dernière.



* |insert_cell_border|  **Créer une cellule** : Permet la création d’une cellule représentée par un polygone à l’intérieur d’une zone. Seul cette zone-cellule pourra contenir des machines. Si la cellule se situe à l’intérieur d’une zone parent, elle sera automatiquement tronquée pour que ses limites correspondent à cette dernière. Il n’est donc pas utile de suivre le contour de l’usine avec précision.

 |design_cell_view| =>  |design_cell_tronq_view|

* |insert_skeleton_point| **Ajouter un point au squelette d'allées** : Permet d’ajouter un point de squelette en double-cliquant dans la zone sélectionnée. Si d’autres points sont déjà présents dans cette zone, la connexion entre ces points, si elle est possible (ligne de connexion interne aux allées et pas « trop proche » des coins des cellules), se fera automatiquement.


* |insert_edge|  **Ajouter une ligne au squelette d'allées** : Permet d’ajouter manuellement une ligne de squelette en double-cliquant sur les deux points qui la composent.


* |input|  **Add IN/OUT** : Permet d’ajouter un point d’entrée/sortie à une zone en double-cliquant sur l’une des bordures de cette zone.


* |nodes|  **View skeleton** : Permet d'afficher le graphe avec les interconnexions. Ce graphe servira de base pour afficher le trafic.


.. * |nodes|  **Show/Hide nodes** : Permet de cacher ou d’afficher les nœuds de connexion au graphe. 

