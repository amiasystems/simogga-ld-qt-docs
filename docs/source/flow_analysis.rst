Analyse graphique des flux
======================================================

.. include:: image.rst
.. include:: icon.rst

|quick_start_3|


Vue graphique 
------------------------------

Sélectionner l’alternative de la vue graphique dans le menu navigation. 

|alternative|

Les machines sont disposées sous forme de cercle pour que tous les flux soient visibles. Cette vue a plusieurs utilités décrites ci-après.

|view_graph|


Outils disponibles 
------------------------------

Via le menu principal Option/Filtre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|menu_option_filter|

* Filtres par machine : permet d’analyser seulement les flux connectés à une ou plusieurs machines

|filter_by_machine|
 
* Filtres par produit : permet de visualiser les flux d’un ou plusieurs produits

|filter_by_product|

Dans l'écran de sélection, chaque produit est représenté par sa proportion de flux (en terme de quantité ou de mouvements) correspondant à l’ensemble de son process par rapport au nombre total de flux.

|filter_by_product_prod|

* Filtres par famille de produit : permet de visualiser les flux relatifs à une ou plusieurs familles de produit quand on sélectionne au préalable une alternative. 

|filter_by_family|

* Filtre Pareto : peut être utilisé sur les quantités de produits transférés ou sur le nombre de transferts réalisés (Mouvements).  

|filter_pareto|

Ces filtres permettent de focaliser l’analyse sur une certaine partie des produits. 
Quelque soit le filtre utilisé, les informations concernant les produits filtrés sont disponibles en bas de la fenêtre. On y retrouve la quantité de pièces concernées ainsi que le nombre de transactions total. 

|filter_info|

Faire toute l’analyse de flux sur un Pareto des produits pourrait biaiser l’étude. Les outils disponibles dans SIMOGGA permettent de faire cette analyse pour la globalité des produits.

Via le panneau en bas de la fenêtre
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Filtre de visualisation : permet de prendre en compte la totalité des produits mais de n’afficher que le flux entre une valeur minimum (Filtre bas) et une valeur maximum (Filtre haut).

|visualisation|

* Slider d'épaisseur : permet de modifier l'épaisseur des flux grâce aux curseurs min et max.

|panel_thickness|


Réorganisation des machines
------------------------------


* Commencer en plaçant le filtre haut à 100% et le filtre bas à 90%. Ce filtre permet de réorganiser les machines avec les plus grands flux en les déplaçant sur la droite de l’écran. 

|panel_filter_1|

|view_graph_0|  =>  |view_graph_1|


* Diminuer le filtre bas pour prendre de plus en plus de flux en charge.

|panel_filter_2|

|view_graph_2|  => |view_graph_3|

* Continuer jusqu'à ce que toutes les machines soient en placées

|view_graph_4|

* Focaliser l'analyse sur une tranche de valeur avec une valeur minimale et maximale et regarder avec une loupe en cochant « Amplifier ». Les flux se différencieront les uns des autres. 

|extend_the_scale|

.. hint:: Cette fonction est très appréciée quand il y a beaucoup de flux dans une fourchette très concentrée. Par exemple, des flux entre 1 et 5000 mais avec une majorité de valeur entre 1000 et 1500. On va placer le « Filtre Bas » à 1000 et le « Filtre Haut » à 1500 de manière à étudier cette partie de flux avec la fonction « Amplifier ».



Reroutage
------------------------------

La représentation des flux donne une idée claire sur les machines liées entre elles. Cette vue ne donne pourtant pas d'information sur l'origine du trafic.

|view_graph_5|

Il existe du trafic entre la machine M6 et la machine M7, mais nous ne savons pas d'où vient ce trafic (niveau N-1) ni où il va (niveau N+1). 

Outil
~~~~~~~~~~~~~~~

|flow_decomposition|

* Analyse : L'outil de reroutage nous permet d'analyser les flux un niveau plus loin. Un flux allant de N-1 à N+1 en passant par N sera représenté par N-1 -> N -> N+1. Dans l'exemple, nous avons le flux connu M6 -> M7 mais nous ne savons pas d'où les produits viennent ni où ils vont.

	L’outil de reroutage va permettre de différencier ces flux. L’écran nous montre que les produits qui passent de M6 vers M7 viennent soit de la machine M5 soit de la machine M7 : M5 -> M6 -> M7 et M7 -> M6 -> M7 

|box_reroutage_sample|

* Reroutage : Cet outil va nous permettre de déplacer toutes les opérations assignées à la machine M6 sur une nouvelle machine M6_1 (machine M6 dupliquée) pour les flux qui viennent de M7 et retourne vers M7.

Création de cellules indépendantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L’idée de base de l’analyse qui suit est de trouver manuellement des groupes de machines indépendantes, c'est à dire supprimer toutes les liaisons/ les flux entre ces groupes de machines (appelées cellules). 

Etant donné que tout ajout/suppression de machines ou réassignation des opérations sur une nouvelle machine implique de créer un nouveau scénario (voir prérequis), il faut commencer par créer un nouveau scénario vierge ou en dupliquer un existant.

Pour créer manuellement ces groupes indépendants, il faut dupliquer certaines machines et réallouer certaines opérations sur cette nouvelle machine. Pour ce faire, il faut utiliser l’outil « Reroutage »

Cette analyse peut être longue en fonction du nombre de flux traités et du nombre de croisement de flux. Une astuce pour décomposer les flux est de procéder de manière systématique.

.. hint:: Dans la liste des machines proposées dans l'outil de reroutage, les cellules sont indiquées (M5|C2). Utilisez cette information pour faciliter la sélection des machines d'« origine » et de « destination ».

* Identifier un flux principal que l’on veut isoler

|view_cell_0|

* |insert_virtual_cell|  Définir une cellule virtuelle autour des machines que l'on veut isoler

|view_cell_1|

.. hint:: Utiliser le bouton |removeMachineFromVirtualCell| pour débloquer les cellules et permettre de déplacer les machines de cellule.


* Commencer par dupliquer les MP (matière première). De cette manière, il ne faut regarder que deux niveaux de flux et pas trois (MP vers toutes les machines). 

|view_cell_2|

* Réassigner tous les flux venant de MP qui ne vont pas vers la branche principale identifiée

|view_cell_3|

|view_cell_4|

* Idem pour PF (produit fini)

|view_cell_5|

|view_cell_6|

* Identifier les flux que l'on veut « casser » entre les cellules et réassigner progressivement chacun d'eux.

|view_cell_7|

* Recommencer pour les autres branches jusqu'à atteindre des cellules indépendantes

|view_cell_10|


Création des cellules automatique
-----------------------------------

Cell optimizer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un algorithme d’optimisation est disponible dans SIMOGGA (Menu principal). 

|menu_optimizer|

Cet algorithme va chercher les groupements optimums des machines du scénario en assignant les opérations sur les machines les plus adéquates pour minimiser les flux entre les cellules. 

* Choisir le scénario de référence : les machines à grouper sont celles définies dans le scénario sélectionné.

|optimizer_open|

	* En sélectionnant le scénario de base, vous avez la possibilité de laisser SIMOGGA dupliquer (1x, 2x) toutes les machines. Seul les machines utiles pour créer des cellules indépendant seront utilisées.

	|box_optimizercell_base|

	* En sélectionnant un autre scénario, SIMOGGA cherchera une solution optimale avec les machines définies dans le scénario

	|box_optimizercell_sc|

* Préciser les différents paramètres

	* Le nombre de duplication de chaque machine (Machines  x nbOccurences)

	* Le nombre de cellules

	* Le nombre maximum de machines par cellule = taille de la cellule

	.. ::hint Des suggestions sont faites en fonction du nombre de machines et du nombre de zones-cellules défini dans le scénario.

	* Définir si les matières premières (MP) et les produits finis (PF) doivent être dupliqués et insérer dans les cellules (pour une meilleure vision de l'indépendance des cellules)

	.. ::attention Les MP/PF ne sont pas pris en compte dans le nombre maximum de machines par cellule, il y aura par contre autant de couple MP/PF qu’il y a de cellules.

* Sélectionner les cas à optimiser parmi les suggestions (« Générer »)

* Insérer de nouveau cas à optimiser en sélectionnant à nouveau un scénario (première liste de cet écran)

* La solution est présentée à l'écran.

|view_optsc|

* La navigation dans les scénarios optimisés se fait via le menu de navigation des scénarios

|panel_lateral_optsc|

Appliquer la solution optimisée
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand les solutions sont créées, elles peuvent être appliquées à l'un ou l'autre des scénarios.

|box_applysolution|

* Sélectionner la solution optimisée à appliquer

* Sélectionner le scénario qui appliquera la solution

* Déterminer si le scénario doit être dupliqué

.. hint:: La solution en cellule est appliquée au niveau de la vue graphique. La solution de reroutage est appliquée à toutes les vues du scénario. Les machines ne seront pas changées de place dans la vue sur plan.

 
Evaluation
------------------------------


La création des cellules va permettre à SIMOGGA de générer des familles de produits assignées à ces cellules.  
Les produits sont alloués à une cellule en fonction du nombre de transactions réalisées au sein de la cellule (trafic intracellulaire). Ils sont présentés avec des couleurs différentes (du vert au rouge) en fonction de la pertinence de leur appartenance à la cellule.

Cette étape est intéressante pour connaître les flux directionnels entre les cellules que l’on voudrait créer dans la vue réelle. 
Les flux intracellulaire (interne aux cellules) sont évalués et présentés en terme de pourcentage des flux totaux.

|table_of_families|




