Prérequis  
==========

.. include:: image.rst
.. include:: icon.rst


|global_screen|


Flux
-----------

* :term:`Flux` = nombre de mouvements ou quantité de produits transférés entre deux machines (From – To). La :term:`vue graphique` représente les flux directionnels (From - To). La :term:`vue réelle` représente les flux additionnés sur chaque segment sans précision de la direction
* Flux exprimé en terme de **mouvement** (mouvement = transfert d’un lot de pièces d’une position A vers une position B) : la valeur du flux correspond au nombre de déplacements effectués pour transférer l’ensemble des pièces d’une position A vers une position B
* Flux exprimé en terme de **quantité** : la valeur du flux correspond à la somme de toutes les pièces transférées d’une position A vers une position B


Machine
-----------

* SIMOGGA considère comme une machine, toute machine, station de travail, stockage où le produit est arrêté, (il peut être transformé par une opération du process ou juste stocké pendant un certain temps).
* Les machines possèdent une **couleur** qui caractérise le type de la machine
* Le niveau de **remplissage** de la partie colorée correspond au pourcentage de la charge par rapport à la capacité

Vues
-----------

* SIMOGGA est organisé en différentes vues : :term:`vue graphique` et :term:`vue réelle`
* Vue **graphique** : visualisation des flux directionnels sans contrainte (culturelles, techniques, historiques)
* Vue **réelle** : Vue avec le plan de l’usine pour tenir compte des contraintes techniques de l’usine (zones de l’usine, entrée-sortie, machine inamovible) 

Scénario / Alternative
---------------------------------

* SIMOGGA est construit sur base d’un jeu de données client = le fichier Excel où chaque opération est assignée à une machine. Cela correspond à une solution Opération-Machine particulière. L’assignation des opérations sur les machines permet de définir la matrice de flux From-To où tous les flux d’une machine A vers une machine B sont représentés.
* Chaque :term:`scénario` présenté dans SIMOGGA correspond à une solution **Opération-Machine** particulière. Cette solution implique des flux entre les différentes machines ainsi qu’une utilisation spécifique des machines (Charge par rapport à la capacité définie). 
* Chaque :term:`scénario` est aussi caractérisé par un **design** d’usine.
* Utilisation des alternatives : Les alternatives sont utilisées pour définir des situations où les machines sont déplacées. Cela permet, à l’intérieur d’un :term:`scénario`, de construire les étapes successives qui seront suivies pour arriver à la situation optimale
* Une :term:`alternative` peut-être vue comme une photo, une sauvegarde à un moment donné. Il suffit de dupliquer l’:term:`alternative` en cours pour continuer le processus d’analyse et en conserver l’état actuel.
