Menu principal
===============

.. include:: image.rst
.. include:: icon.rst


SIMOGGA
---------


Fichier
--------
 
|menu_file|

* **Ouvrir** : Ouvre un cas sur base d’un fichier de données au format xml.

|menu_file_open|

* **Sauvegarder** : Sauvegarde le cas en modifiant le fichier de données chargé.

|menu_file_save|

* **Sauvegarder sous** : Sauvegarde en donnant la possibilité de choisir le nom et l’emplacement du fichier xml.

|menu_file_save_as|

* **Fermer** : Ferme le cas ouvert. Il sera au préalable demandé si une sauvegarde des éventuelles modifications effectuées sur le cas courant est nécessaire.

|menu_file_close|

* **Quitter** : Ferme l’application. Il sera au préalable demandé si une sauvegarde des éventuelles modifications effectuées sur le cas courant est nécessaire. 

|menu_file_quit|

Scénario
-----------

|menu_scenario|
 
* **Nouveau scénario** : Crée un nouveau scénario vierge avec une alternative de chaque type.

|menu_scenario_new_sce|

* **Dupliquer scénario** : Duplique le scénario courant.

|menu_scenario_dup_sce|

* **Supprimer scénario** : Supprime le scénario courant et toutes ses alternatives.

|menu_scenario_del_sce|

* **Nouvelle alternative** : Crée une nouvelle alternative vierge.

|menu_scenario_new_alt|

* **Dupliquer alternative** : Duplique l'alternative courante. 

|menu_scenario_dup_alt|

* **Supprimer alternative** : Supprime l'alternative courante.

|menu_scenario_del_alt|

* **(Dé)sélectionner l'alternative comme référence** : L’alternative courante est enregistrée comme alternative de référence au niveau du dashboard. Une seule alternative peut être indiquée comme référence. Toutes les autres alternatives seront comparées à cette alternative de référence.

|menu_scenario_as_is|
.. Set/Unset Alternative as Baseline

Options
---------

|menu_option|
 
* **Panneau de configuration** : Affiche le panneau contenant les paramètres personnalisables de SIMOGGA.

|menu_option_conf|

* **Reroutage** : Affiche le panneau permettant de procéder au reroutage des produits pour modifier les flux entre machines. Cela ne peut pas se faire sur le scénario de référence (Base).

|menu_option_conf|

* **Filtres** : Affiche le panneau permettant d’effectuer des filtres sur les produits et sur les machines.

|menu_option_filter|

Optimizer
------------

|menu_optimizer|

* Cell Optimizer : Affiche le panneau pour la génération de cellules indépendantes.

|menu_optimizer_cell|

* Appliquer Solution : Permet d’appliquer une solution optimisée sur l'alternative courante en précisant si le scénario doit être dupliqué ou non.

|menu_optimizer_apply|

Langue
---------

Choix des langues.
Langues disponibles : Francais, Anglais, Allemand.

Aide
---------

