Glossary
========

.. glossary::
	:sorted:
	
	AS-IS
		La situation AS-IS est l'image de l'usine actuellement. Représenter cette situation actuelle va permettre de la quantifier et de l'utliser comme référence pour comparer tous les futurs scénarios.

	
	Alternative
		Elles  utilisées pour définir des situations où les machines sont déplacées. Cela permet, à l’intérieur d’un scénario, de construire les étapes successives qui seront suivies pour arriver à la situation optimale
		Une alternative peut-être vue comme une photo, une sauvegarde à un moment donné. Il suffit de dupliquer l’alternative en cours pour continuer le processus d’analyse et en conserver l’état actuel.
	
	Flux
		Nombre de mouvements ou quantité de produits transférés entre deux machines (From – To). 
		Un flux est exprimé en terme de mouvement et de quantité.

	KPI
		Key Performance Indicator : Critère d'évaluation des solutions

	Machine
		SIMOGGA considère comme une machine, toute machine, station de travail, stockage où le produit est arrêté, (il peut être transformé par une opération du process ou juste stocké pendant un certain temps).
		Les machines possèdent une **couleur** qui caractérise le type de la machine
		Le niveau de **remplissage** de la partie colorée correspond au pourcentage de la charge par rapport à la capacité

	Mouvement (Flux)
		Un mouvement est un transfert d’un lot de pièces d’une position A vers une position B
		La valeur du flux correspond au nombre de déplacements effectués pour transférer l’ensemble des pièces d’une position A vers une position B

	Quantité (Flux)
		La valeur du flux correspond à la somme de toutes les pièces transférées d’une position A vers une position B

	Scénario 
		Une solution Opération-Machine qui implique des flux entre les différentes machines ainsi qu’une utilisation spécifique des machines (Charge par rapport à la capacité défini). 
		Chaque scénario est caractérisé par un design d’usine.

	Scène
		la scène défini la zone d'affichage graphique.

	Vues
		SIMOGGA est organisé en différentes vues : vue graphique et vue réelle
		
	Vue Graphique
		La vue graphique représente les flux directionnels (From - To)
		La vue graphique permet la visualisation des flux directionnels sans contrainte (culturelles, techniques, historiques)

	Vue réelle
		La vue réelle représente les flux additionnés sur chaque segment sans précision de la direction
		La vue réelle inclut le plan de l’usine pour tenir compte des contraintes techniques de l’usine (zones de l’usine, entrée-sortie, machine inamovible) 

		


