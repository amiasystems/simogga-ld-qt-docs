Traitement d'un cas complet industriel
==========================================

.. include:: image.rst
.. include:: icon.rst

Ouverture et fermeture d’un cas
-----------------------------------

Ouverture d’un cas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Lors d’un l’ouverture d’un cas, via la commande «Ouvrir » du menu « Fichier », une boite de dialogue apparaitra dans laquelle il est demandé à l’utilisateur si il faut créer les MP(Matières premières)/PF(Produits finis) si ceux-ci n’existent pas déjà. Si la réponse donnée est oui, deux machines supplémentaires seront créées : une MP et une PF. Dans le cas contraire, les MP/PF ne seront pas présents. 

|message_load_case|

.. hint:: Si l’utilisateur charge un cas ayant déjà été sauvegardé alors qu’il contenait des MP/PF, ceux-ci apparaitront même si l’utilisateur choisit de ne pas les créer. En effet, ayant déjà été créées au préalable, elles font désormais partie des machines traitées dans le cas présent. 
 
Si SIMOGGA ne parvient pas à charger le fichier xml sélectionné, une fenêtre d’erreur apparaitra pour le signifier à l’utilisateur. Pour que SIMOGGA soit capable de charger un fichier xml, la structure de celui-ci doit correspondre à la structure mise en place par SIMOGGA. 

|message_dialog_openfail|

Vérifier que le fichier xml ne contient pas de caractères spéciaux tels que : 

* #DIV/0!

* #N/A

* #VALEUR!

* /

* \*

* '

* (  ou  )

* &

 
Sauvegarde d’un cas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deux types de sauvegardes sont possibles et se retrouvent dans le menu « Fichier » :

* Sauvergarder : la sauvegarde se fera sur le fichier ouvert. Les modifications seront sauvées à même le fichier xml. 

|menu_file_save|

* Sauvergarder sous… : L’utilisateur a la possibilité de choisir l’emplacement de la sauvegarde et le nom du fichier sauvegardé. 

|menu_file_save_as|


Fermeture d’un cas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|menu_file_close|

Lors de la fermeture d’un cas à l’aide la commande « Fermer » du menu « Fichier », il sera demandé à l’utilisateur s’il souhaite au préalable sauvegarder les modifications qu’il a effectuées.

|message_close|

S’il répond par l’affirmative, le cas actuellement traité sera sauvegardé dans le même fichier xml.  Dans le cas contraire, les modifications effectuées depuis le chargement du cas seront perdues. 


Création de la situation de référence
----------------------------------------

Création de l’usine – Mode Design
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dans la vue réelle, il est possible d’obtenir une représentation en deux dimensions de la zone de travail correspondant à la réalité. 

* Sélectionner une alternative réelle en cliquant sur l'alternative dans le menu scénario

|panel_lateral_scenario_select_rv|

* Passer en mode design

|design_selection|

* Sélectionner d’une image d’arrière-plan correspondant au plan de l’usine (png ou jpeg) via le bouton |insert_factory_plan|

|process_real_1_map|
 
* Adapter la taille du plan 

	* Cliquer sur le plan pour l’activer
	* Modifier la taille du plan via le zoom avec la roulette de la souris
	* Il faut que la taille de la machine de référence corresponde à la taille des machines sur le plan.

* Déterminer les contours de l’usine 

|process_real_2_factory|

	* Info : Panneau latéral de navigation

	|panel_lateral_structure_select_comp|

	* Cliquer sur l'icone |insert_factory_border|

	* Créer le contour par de simples clics successifs sur les coins du plan. Chaque clic de souris correspond à un point du polygone représentant ce contour.

	* Terminer par un double clic (sur l'avant dernier point) pour indiquer que le polygone est complet et il se fermera automatiquement. 

	* Choisir le type de zone voulue (site, batiment, étage, aire)

	* Renommer la zone


* Déterminer des sous-zones, si nécessaire

	* **Sélectionner la zone parent** via un clic sur la zone au niveau de la scène ou au niveau du menu de navigation dans la structure.

	|panel_lateral_structure_select_site|

	* Cliquer sur l'icone |insert_factory_border|

	* Créer le contour par de simples clics successifs sur les coins du plan. Chaque clic de souris correspond à un point du polygone représentant ce contour.

	* Terminer par un double clic (sur l'avant dernier point) pour indiquer que le polygone est complet et il se fermera automatiquement. 

	* Choisir le type de zone voulue (site, batiment, étage, aire)

	* Renommer la zone

	* Tronquage automatique de cette nouvelle zone par rapport à la zone parent. Il n'est donc pas utile d'être précis dans le dessin au niveau des limites de la zone parent. 

* Déterminer les zones de travail où pourront être placées les machines. 

|process_real_3_cell|

	.. hint:: une zone de travail peut avoir comme « parent » n'importe quelle zone définie précédemment (site, batiment, étage, aire)

	* Sélectionner la zone parent via un clic sur la zone au niveau de la scène ou au niveau du menu de navigation dans la structure.

	|panel_lateral_structure_select_building|

	* Cliquer sur l'icone |insert_cell_border|

	* Si la zone parent n'est pas sélectionnée, un message apparait. 

	|message_parent_selection|

	.. hint:: Il faut alors sélectionner le parent dans le menu latéral de navigation. La scène n'est pas accessible tant que le bouton |insert_cell_border| est enfoncé.

	* Créer un polygone à l’aide d’une succession de clic de souris 

	* Terminer par un double clic qui marquera la fermeture automatique du polygone 

	* Si le polygone créé dépasse de la zone dans laquelle il se trouve, il sera tronqué pour correspondre à son contour. 
 
 .. hint:: maintenir la touche Ctrl/Cmd enfoncée pour créer plusieurs zones sans devoir cliquer si l'icone à chaque fois.


* Mise à l'échelle
	
	* Cliquer sur le bouton  |scale|

	* Utiliser une distance connue sur le plan

	* Cliquer (simple clic) sur les deux points définissant cette distance

	|set_scale|

	* Définir la distance en mètre

	* Cliquer sur Ok


Création du squellete d'allées
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Le squelette est le graphe représentant les allées de l'usine par lequel va pouvoir passer le trafic entre les zones de travail (trafic intercellulaires). Cette outil est disponible dans le mode design de l'alternative réelle.

|process_real_4_skeleton|
	
* Sélectionner la zone où l'on veut mettre des allées via la scène ou le menu de navigation

* Cliquer sur l'icone  |insert_skeleton_point| 
	
* Insérer les point de connexion des allées à l'aide de doubles clics 

* Connection automatique par SIMOGGA des points entre eux par des lignes si

	* la ligne ne traverse pas de zones 

	* les lignes ne passent pas trop près des coins de zones

	* les lignes ne sont pas trop proches

* Ajouter des lignes non créées automatiquement

	* Activer le bouton  |insert_edge|

	* Double clic sur les deux points à connecter

* Supprimer des lignes non voulues

	* Sélectionner l'élement à supprimer

	* Sélectioner plusieurs élements en maintenant la touche Ctrl/Cmd enfoncée

	* Cliquer sur le bouton |remove_any_element|
 
	* Cliquer sur Ok

.. Il est également possible de générer le squelette de manière totalement automatique à l’aide du bouton « create automatic skeleton » de la barre d’outils. Si cette option est choisie, SIMOGGA tentera de générer un squelette couvrant l’intégralité des zones intercellules. 

* Ajouter des entrées et sorties (point IO) en bordure d’une zone. 
	
	|process_real_5_io|

	* Activer le bouton  |input|

	* Double cliquer sur la bordure de la zone

	* Clic droit sur le point ajouté pour modifier son type.
		* IN : Seuls les flux entrants pourront passer. 
		* OUT : Seuls les flux sortants pourront passer.
		* I/O : Tous les flux pourront passer. 

.. hint:: Les points IO seront utiles dans le cas où on se trouve face à une usine comportant plusieurs étages et/ou bâtiments, ou si l’on souhaite travailler sur plusieurs sites simultanément. 
 

* Fixer une distance sur une ligne de squelette. 
		
		.. hint:: Ce sera particulièrement utile lorsque l’on souhaite travailler avec plusieurs sites simultanément, une simple ligne de squelette pouvant représenter un nombre définis de mètres ou kilomètres. 

		|process_real_7_setvalue|

		* Clic droit sur la ligne à fixer 

		* Sélectionner l'option "Fixer la distance"

		* Rentrer une valeur en mètres ou kilomètres

		* Clic droit sur la ligne pour afficher la valeur entrée

		* Supprimer la valeur via l'option "Reset"
 
 
Placement des machines – Mode Interaction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quand des zones de travail (en jaune) ont été créées dans le mode design, les machines peuvent y être placées. Pour ce faire, un simple "drag & drop" suffit. On peut dès lors représenter la situation initiale dans laquelle se trouve l’usine.
 

Différent type de visualisation sont disponible :

* |visualisation_standard|  standard : les machines sont connectées en direct si elles appartiennent à la même zone, sinon le trafic se fait par les allées

* |visualisation_byalleys| par les allées seulement : tout le trafic passent par les allées

* |visualisation_crowfly| à vol d'oiseau : le graphe du squelette d'allées n'est pas utilisé. Les machines sont connectées entre elle via des flux directionnels



Dans les deux premières visualisations, la notion de direction disparait. C'est la totalité du trafic qui est représenté sur chaque segment.
Ce mode de visualisation n'est possible que si un squelette d'allée a été construit dans le mode design. Dans le cas contraire, la représentation se fait à vol d'oiseau.




Les machines peuvent être modifiées par : 

* leur position via le "drag and drop"

* leur dimension pour représenter avec plus de précision la place réelle que prend la machine au sein de l’usine :

|process_real_9_machine_resize|

	* Sélectionner la machine

	* Affichage des carrés sur le contour

	* Déplacer l'un des des carrés du contour
 
* leur orientation :
	
	* Cliquer sur le bouton "Rotation" |rotate_machine|

	* Double clic sur la machine

* leur immobilisation. Une machine cadenasée ne pourra plus bouger :
	
	|process_real_10_machine_lock|

	* Cliquer sur le bouton |lock_machine|

	* Double clic sur la machine

	* Affichage du cadenas sur la machine

	

Analyse des flux dans la vue graphique
------------------------------------------

La vue graphique offre une représentation des machines et des flux directionnels (trafics) qui passent entre celles-ci sans prendre en compte le design de l’usine, les contraintes techniques, historiques ou culturelles.

Cette vue permet la réprésentation du diagramme spaghetti, un ensemble de flux entremelés. Différents outils sont proposés pour analyser ces flux.

L'objectif de cette analyse est de mettre en évidence les familles de produits en définissant les flux principaux, en les isolant et en créant des groupes de machines (cellules) dédicacées à ces flux. 

Ce processus s’effectue en deux étapes. 

1.	Réorganisation des machines pour dégager les différents axes de flux. 

2.	Duplication et réassignation pour s’approcher d’une solution « lean ».

Réorganisation des machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Outils à utiliser :

* Filtre du graphe

* Filtres des produits

* Epaisseur des flux

Au chargement, la vue graphique présente les machines disposées sur plusieurs cercles successifs. De manière générale, il ne sera pas aisé de distinguer avec précision la manière dont s’organisent les flux.
 
|process_graph1|

Il sera dès lors nécessaire d’utiliser les outils mis à disposition de l’utilisateur afin d’organiser les flux de manière plus visuelle. Pour ce faire, il sera possible : 

* D’utiliser les filtres permettant d’occulter les flux les moins importants via le panneau de filtre en bas de l'écran

|panel_filter|

|process_graph2|

* D’utiliser les filtres produits : 
 
|process_graph3|

Il sera alors possible de ne gérer qu’une partie des machines et de faire progressivement apparaitre les flux de moindre importance. Au final, après avoir réorganisé la position de chaque machine, on obtient une vision plus parlante de la situation des flux. 
 
|process_graph6|

Sur l’image ci-dessus, on distingue clairement un axe vertical central. Cependant, une multitude de flux viennent encore croiser cet axe et il reste compliqué de déterminer de potentielles familles de produits. Ce sera l’objet de la seconde étape qui consiste à dupliquer certaines machines et de réassigner les flux sur celles-ci. 

Duplication et réassignation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Outils à utiliser :

* Filtre du graphe

* Filtres des produits

* Epaisseur des flux

* Panneau de reroutage

Pour dupliquer les machines, il faudra au préalable créer un nouveau scénario sur base de celui que l’on vient de traiter. Une fois cela fait, il est possible, via un clic droit, de dupliquer chaque machine. 
 
|machine_rc_base|

Une fois qu’une machine a été dupliquée, il sera alors possible, via le panneau de réassignation des flux, de modifier les flux passant par cette machine. 

|process_graph8|

En sélectionnant l’ancienne machine, la nouvelle machine, ainsi que les deux machines par lesquels arrivent et repartent les flux, il est possible de modifier le cheminement de ces flux pour les faire passer par la nouvelle machine sélectionnée. 

Dans un premier temps, il est intéressant de se concentrer sur les MP(matières premières)/PF(produits finis) afin de dégager plusieurs axes potentiels. Dans l’exemple ci-dessous, on les a toutes les deux dupliquées 2x. 
 
|process_graph9|

On peut dès lors remarquer que l’on s’oriente vers trois axes verticaux. Cependant, de nombreux flux traversent encore ces axes de manière horizontale. La prochaine étape sera alors de dupliquer les machines comprenant la plus grande partie de ces flux. 
Dans l’exemple ci-dessous, les machines 8 et 14 ont été dupliquées. On remarque que, rapidement, l’axe de droite est devenu presque complètement indépendant des deux autres. Par contre, l’axe de gauche et l’axe central sont encore trop mélangés. 

|process_graph10|

En dupliquant plusieurs machines supplémentaires, on arrive néanmoins à isoler ces deux parties de manière presque totale. 
 
|process_graph11|

Nous avons à présent décomposé les flux de manière à obtenir trois axes distincts. 

Désormais, il est possible de créer des « familles de produits » via le bouton |insert_virtual_cell| en entourant chaque axe à l’aide d’une cellule virtuelle. 

Il est alors possible de consulter le tableau des produits par cellules afin de pouvoir dégager les familles de produits créées. Cliquer sur |table_of_families|
 
|process_graph13|

Chaque cellule contient l'ensemble des produits qui lui ont été assignés. Chaque produit est accompagné d'un paramètre d'assignation (pourcentage) qui caractérise l'assignation. Plus les transactions d'un produit se font à l'intérieur de la cellule, plus ce pourcentage  sera élevé. 

.. hint:: Si une opération se fait dans une autre cellule, le paramètre d'assignation diminuera différement si c'est la première ou la dernière opération (impliquant un seul mouvement externe à la cellule) ou une opération dans le process (impliquant un aller-retour à l'extérieur). De même, si cette opération fait partie d'un long process (10 opérations), le paramètre d'assignation sera plus élevé.


Mise en cellules
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La vue cellule permet une représentation cellulaire des flux. A la différence des cellules virtuelles qu’il est possible de créer au sein de la vue graphique, les cellules ont cette fois-ci des points d’entrées/sorties par lesquels passeront les flux intercellulaires. Lors de l’utilisation de RRPCMGGA pour créer des solutions optimisées, ce seront des vues cellules qui seront générées. 
Dans une situation idéale, la totalité des flux devrait être intracellulaires, comme sur l’exemple ci-dessous. 
 
|process_cell1|

Cependant, dans la plupart des cas, une partie de flux intercellulaire sera présente. Comme dit précédemment, ils passeront alors par les points IN/OUT. Il est possible de déplacer librement ces points sur le contour de la cellule, tout comme il est également possible de modifier la taille de la cellule. Les machines qui s’y trouvent s’y déplaceront de manière automatique pour rester sur le cercle qu’elles forment. La taille minimale des cellules est définie par le cercle des machines qu’elle contient. Les machines ne pouvant se chevaucher, il sera impossible de diminuer davantage la taille de la cellule si les machines se trouvant à l’intérieur entrent en collision. 

|process_cell2|

L’utilisateur peut à tout moment déplacer les machines mais elles sont, par défaut, bloquées à l’intérieur de leur cellule. Il est néanmoins possible de retirer cette limitation en activant le bouton « lock/unlock machines » de la barre d’outils. 
Enfin, l’utilisateur peut, tout comme en vue graphique, avoir accès au tableau des produits par cellules. 
 
|process_cell3|


Création de scénarios
-------------------------

Comment utiliser les scénarios et alternatives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|use_scenarios|

Pour créer des solutions sur base de la réassignation des flux réalisée dans la vue graphique du « Scénario 2 » :

* Travailler sur le Scenario 2, alternative « S2RA1 » 

* Déterminer des changements incrémentaux (en terme de positionnement de machines) : 
	* Modifier « S2RA1 »
	* Dupliquer « S2RA1 » en « S2RA2 »
	* Modifier « S2RA2 »
	* Dupliquer « S2RA2 » en « S2RA3 »
	* Modifier « S2RA3 » 

..hint chaque état que l'on veut sauver doit être dupliqué pour continuer les modifications.

* Modifier le design de l'usine
	* Dupliquer le scénario 2 (= Scénario 3)
	* Modifier le design (supprimer des cellules, créer de nouvelles cellules...)
	* Supprimer les alternatives précédentes non utiles du scénario 2 (« S2RA1 » et « S2RA2 »)
	* Commencer à modifier l'alternative « S3RA3 » (nouveau design avec les machines positionnées comme dans la dernière alternative du scénario 2)
	* Procéder aux changements incrémentaux pour créer les différentes alternatives « S3RA3 », « S3RA4 » en « S3RA5 »

Les modifications incrémentales (via les alternatives) peuvent aboutir à la libération d'une zone pour ensuite refaire le design de cette zone (via un nouveau scénario).


Utilisation du Dashboard
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

|process_real_11_dashboard|

Le dashboard permet de comparer différentes alternatives réelles afin de visualiser rapidement et simplement quelle est la meilleure d’entre elles. Il contient plusieurs données :

* Le nom de l’alternative et son scénario

* Le cout total que représente la situation

* Le pourcentage de gain de cette situation par rapport à celle indiquée comme la référence

* Le nombre total de kilomètres que parcourent les produits

* Le temps total écoulé

Pour déterminer quelle est l’alternative de référence, il faut soit passer par le menu Scénario, soit effectuer un clic droit sur l’alternative sélectionnée et choisir l’option « (Dé)sélectionner l'atrenative comme référence ». Le calcul du pourcentage de gains des autres alternatives se fera alors sur base de cette alternative de référence. 
Les gains par rapport à la situation de référence sont indiqués en vert (diminuation de 24,4% du trafic) tandis que les pertes sont en rouge (augmentation de 20,6%).




