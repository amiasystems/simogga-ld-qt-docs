.. -----------------------------prise_fr_main-----------------------------------
.. |prise_en_main| image:: _static/img/prise_en_main_rapide_fr.png
.. |quick_start| image:: _static/img/quick_start_fr.png
.. |quick_start_1| image:: _static/img/quick_start_1_fr.png
.. |quick_start_2| image:: _static/img/quick_start_2_fr.png
.. |quick_start_3| image:: _static/img/quick_start_3_fr.png
.. |quick_start_4| image:: _static/img/quick_start_4_fr.png
.. |global_screen| image:: _static/img/global_screen_fr.png
 

.. list of all pictures / Fichier excel
.. |products| image:: _static/img/excel_products_fr.png
.. |machines| image:: _static/img/excel_machines_fr.png
.. |machine_type| image:: _static/img/excel_machine_type_fr.png
.. |open_file| image:: _static/img/menu_file_open_fr.png
.. |message_load_case| image:: _static/img/message_load_case_fr.png

.. list of all pictures / Création de la situation As-IS
.. |design_selection| image:: _static/img/mode_design_selection_fr.png
.. |interaction_selection| image:: _static/img/mode_interaction_selection_fr.png
.. |scenario| image:: _static/img/scenario_active_fr.png
.. |alternative| image:: _static/img/alternative_active_gv_fr.png
.. |set_as_is_situation| image:: _static/img/alternative_asis_fr.png
.. |set_as_is_situation_by_scenario| image:: _static/img/menu_scenario_set_as_is_fr.png
.. |set_scale| image:: _static/img/set_scale_fr.png
.. |dashboard| image:: _static/img/dashboard_fr.png

.. list of all pictures / Analyse graphique des flux
.. |filter_by_machine| image:: _static/img/box_filter_machine_fr.png
.. |filter_by_product| image:: _static/img/box_filter_product_fr.png
.. |filter_by_product_prod| image:: _static/img/box_filter_product_product_fr.png
.. |filter_by_family| image:: _static/img/box_filter_product_family_fr.png
.. |filter_pareto| image:: _static/img/box_filter_product_pareto_fr.png
.. |filter_info| image:: _static/img/box_filter_product_info_fr.png
.. |visualisation| image:: _static/img/panel_lateral_filter_fr.png
.. |visualisation_first_step| image:: _static/img/visualisation_first_step_fr.png
.. |visualisation_next_step| image:: _static/img/visualisation_next_step_fr.png
.. |extend_the_scale| image:: _static/img/extend_scale_fr.png
.. |flow_decomposition| image:: _static/img/box_rerouting_fr.png
.. |optimizer_open| image:: _static/img/box_optimizer_open_fr.png
.. |optimizer| image:: _static/img/box_optimizer_fr.png

.. list of all pictures / Génération de scénarios
.. |evaluation_positive| image:: _static/img/evaluation_positive_fr.png
.. |evaluation_negative| image:: _static/img/evaluation_negative_fr.png
.. |use_scenarios| image:: _static/img/use_scenarios.png

.. -----------------------------main principal-----------------------------------

.. list of all pictures // menu_principal

.. list of all pictures / File
.. |menu_file| image:: _static/img/menu_file_fr.png
.. |menu_file_open| image:: _static/img/menu_file_open_fr.png
.. |menu_file_save| image:: _static/img/menu_file_save_fr.png
.. |menu_file_save_as| image:: _static/img/menu_file_saveas_fr.png
.. |menu_file_close| image:: _static/img/menu_file_close_fr.png
.. |menu_file_quit| image:: _static/img/menu_file_quit_fr.png

.. list of all pictures / Option
.. |menu_option| image:: _static/img/menu_options_fr.png
.. |menu_option_conf| image:: _static/img/menu_options_conf_fr.png
.. |menu_option_reroutage| image:: _static/img/menu_options_rerouting_fr.png
.. |menu_option_filter| image:: _static/img/menu_options_filter_fr.png

.. list of all pictures / scenario
.. |menu_scenario| image:: _static/img/menu_scenario_fr.png
.. |menu_scenario_new_sce| image:: _static/img/menu_scenario_new_sce_fr.png
.. |menu_scenario_dup_sce| image:: _static/img/menu_scenario_dup_sce_fr.png
.. |menu_scenario_del_sce| image:: _static/img/menu_scenario_del_sce_fr.png
.. |menu_scenario_new_alt| image:: _static/img/menu_scenario_new_alt_fr.png
.. |menu_scenario_dup_alt| image:: _static/img/menu_scenario_dup_alt_fr.png
.. |menu_scenario_del_alt| image:: _static/img/menu_scenario_del_alt_fr.png
.. |menu_scenario_as_is| image:: _static/img/menu_scenario_set_as_is_fr.png

.. list of all pictures / optimizer
.. |menu_optimizer| image:: _static/img/menu_optimizer_fr.png
.. |menu_optimizer_cell| image:: _static/img/menu_opt_cell_fr.png
.. |menu_optimizer_apply| image:: _static/img/menu_opt_apply_fr.png

.. -----------------------------menu_lateral-----------------------------------

.. list of all pictures / menu_lateral
.. |panel_lateral| image:: _static/img/panel_lateral_fr.png
.. |panel_lateral_scenario| image:: _static/img/panel_lateral_scenario_fr.png
.. |scenario_right_click| image:: _static/img/scenario_fr.png
.. |alternative_right_click| image:: _static/img/alternative_fr.png
.. |panel_lateral_structure| image:: _static/img/panel_lateral_structure_fr.png
.. |panel_lateral_info| image:: _static/img/panel_lateral_info_fr.png
.. |panel_lateral_dashboard| image:: _static/img/panel_lateral_dashboard_fr.png
.. |panel_filter| image:: _static/img/panel_lateral_filter_fr.png

.. |panel_lateral_optsc| image:: _static/img/panel_lateral_optsc_fr.png
.. |view_optsc| image:: _static/img/view_optsc_fr.png


.. |view_graph| image:: _static/img/view_graph_fr.png
.. |view_graph_0| image:: _static/img/view_graph_0.png
.. |view_graph_1| image:: _static/img/view_graph_1.png
.. |view_graph_2| image:: _static/img/view_graph_2.png
.. |view_graph_3| image:: _static/img/view_graph_3.png
.. |view_graph_4| image:: _static/img/view_graph_4.png
.. |view_graph_5| image:: _static/img/view_graph_5.png
.. |panel_filter_1| image:: _static/img/panel_lateral_filter_1_fr.png
.. |panel_filter_2| image:: _static/img/panel_lateral_filter_2_fr.png

.. |panel_filter_60-100| image:: _static/img/panel_lateral_filtre_60-100_fr.png
.. |panel_filter_25-100| image:: _static/img/panel_lateral_filtre_25-100_fr.png
.. |panel_filter_0-25| image:: _static/img/panel_lateral_filtre_0-25_fr.png
.. |panel_filter_0-25_magn| image:: _static/img/panel_lateral_filtre_0-25_magn_fr.png
.. |panel_filter_0-100_view| image:: _static/img/panel_lateral_filtre_0-100_view.png
.. |panel_filter_60-100_view| image:: _static/img/panel_lateral_filtre_60-100_view.png
.. |panel_filter_25-100_view| image:: _static/img/panel_lateral_filtre_25-100_view.png
.. |panel_filter_0-25_view| image:: _static/img/panel_lateral_filtre_0-25_view.png
.. |panel_filter_0-25_view_magnify| image:: _static/img/panel_lateral_filtre_0-25_magnify_view.png
.. |panel_thickness| image:: _static/img/thickness_fr.png

.. -----------------------------menu vue-----------------------------------

.. list of all pictures / menu_vue / graphique
.. |panel_graphic| image:: _static/img/panel_view_graphic_fr.png

.. list of all pictures / menu_vue / sur plan/Reelle
.. |panel_real_interaction| image:: _static/img/panel_view_real_interaction_fr.png
.. |panel_real_design| image:: _static/img/panel_view_real_design_fr.png

.. -----------------------------menu_sur_vue-----------------------------------

.. list of all pictures / menu_sur_vue
.. |machine| image:: _static/img/machine.png
.. |machine_selected| image:: _static/img/machine_selected.png
.. |machine_rc_base| image:: _static/img/machine_rigth_click_base_fr.png
.. |machine_rc_ref| image:: _static/img/machine_rigth_click_notdel_fr.png
.. |machine_rc_dup| image:: _static/img/machine_rigth_click_fr.png


.. |machine_duplicate| image:: _static/img/machine_rigth_click_dup_fr.png
.. |machine_delete| image:: _static/img/machine_rigth_click_del_fr.png
.. |machine_resized| image:: _static/img/machine_resize.png
.. |ref_machine_not_delete| image:: _static/img/machine_rigth_click_notdel_fr.png
.. |machine_locked| image:: _static/img/machine_locked.png

.. |design_cell_view| image:: _static/img/process/factory_design_cell_view.png
.. |design_cell_tronq_view| image:: _static/img/process/factory_design_cell_tronq_view.png

.. -----------------------------box main menu-----------------------------------

.. list of all pictures / box main menu / parameter
.. |box_parameters| image:: _static/img/box_parameters_fr.png

.. list of all pictures / box main menu / reroutage
.. |message_reroutage_base_case| image:: _static/img/message_rerouting_base_case_fr.png
.. |box_reroutage_open| image:: _static/img/box_rerouting_fr.png
.. |box_reroutage_sample| image:: _static/img/box_rerouting_sample_fr.png
.. |box_reroutage_selection| image:: _static/img/box_rerouting_fr.png
.. |box_reroutage_selection_old| image:: _static/img/box_rerouting_old_fr.png
.. |box_reroutage_selection_new| image:: _static/img/box_rerouting_new_fr.png
.. |box_reroutage_selection_from| image:: _static/img/box_rerouting_from_fr.png
.. |box_reroutage_selection_to| image:: _static/img/box_rerouting_to_fr.png

.. list of all pictures / box main menu / filter
.. |box_filter_product| image:: _static/img/box_filter_product_fr.png
.. |box_filter_product_product| image:: _static/img/box_filter_product_product_fr.png
.. |box_filter_product_family| image:: _static/img/box_filter_product_family_fr.png
.. |box_filter_product_pareto| image:: _static/img/box_filter_product_pareto_fr.png
.. |box_filter_product_info| image:: _static/img/box_filter_product_info_fr.png
.. |box_filter_machine| image:: _static/img/box_filter_machine_fr.png

.. list of all pictures / box main menu / cell optimizer
.. |box_optimizer_open| image:: _static/img/box_optimizer_open_fr.png
.. |box_optimizercell| image:: _static/img/box_optimizercell_fr.png
.. |box_optimizercell_base| image:: _static/img/box_optimizer_base_fr.png
.. |box_optimizercell_sc| image:: _static/img/box_optimizer_sc_fr.png

.. list of all pictures / box main menu / apply solution
.. |box_applysolution| image:: _static/img/box_optimizer_apply_fr.png

.. -----------------------------traitement complet-----------------------------------

.. list of all pictures / traitement complet /
.. |message_dialog_openfail| image:: _static/img/message_dialog_openfail_fr.png
.. |message_close| image:: _static/img/message_close_fr.png
.. |panel_lateral_scenario_select_rv| image:: _static/img/panel_lateral_scenario_select_rv_fr.png
.. |message_parent_selection| image:: _static/img/message_parent_selection_fr.png
.. |panel_lateral_structure_select_comp| image:: _static/img/panel_lateral_structure_select_comp.png
.. |panel_lateral_structure_select_site| image:: _static/img/panel_lateral_structure_select_site.png
.. |panel_lateral_structure_select_building| image:: _static/img/panel_lateral_structure_select_building.png

.. |process_real_1_map| image:: _static/img/process/vue_real/process_real_1_map.png
.. |process_real_2_factory| image:: _static/img/process/vue_real/process_real_2_factory.png
.. |process_real_3_cell| image:: _static/img/process/vue_real/process_real_3_cell.png
.. |process_real_4_skeleton| image:: _static/img/process/vue_real/process_real_4_skeleton.png
.. |process_real_5_io| image:: _static/img/process/vue_real/process_real_5_io.png
.. |process_real_6_scale| image:: _static/img/process/vue_real/process_real_6_scale_fr.png
.. |process_real_7_setvalue| image:: _static/img/process/vue_real/process_real_7_setvalue_fr.png
.. |process_real_8_graph| image:: _static/img/process/vue_real/process_real_8_graph.png
.. |process_real_9_machine_resize| image:: _static/img/process/vue_real/process_real_9_machine_resize.png
.. |process_real_10_machine_lock| image:: _static/img/process/vue_real/process_real_10_machine_lock.png
.. |process_real_11_dashboard| image:: _static/img/process/vue_real/process_real_11_dashboard_fr.png

.. |process_graph1| image:: _static/img/process/vue_graph/graph1.png
.. |process_graph2| image:: _static/img/process/vue_graph/graph2.png
.. |process_graph3| image:: _static/img/process/vue_graph/graph3.png
.. |process_graph4| image:: _static/img/process/vue_graph/graph4.png
.. |process_graph5| image:: _static/img/process/vue_graph/graph5.png
.. |process_graph6| image:: _static/img/process/vue_graph/graph6.png
.. |process_graph7| image:: _static/img/process/vue_graph/graph7.png
.. |process_graph8| image:: _static/img/process/vue_graph/graph8.png
.. |process_graph9| image:: _static/img/process/vue_graph/graph9.png
.. |process_graph10| image:: _static/img/process/vue_graph/graph10.png
.. |process_graph11| image:: _static/img/process/vue_graph/graph11.png
.. |process_graph12| image:: _static/img/process/vue_graph/graph12.png
.. |process_graph13| image:: _static/img/process/vue_graph/graph13.png

.. |process_cell1| image:: _static/img/process/vue_cell/cell1.png
.. |process_cell2| image:: _static/img/process/vue_cell/cell2.png
.. |process_cell3| image:: _static/img/process/vue_cell/cell3.png

.. |view_cell_0| image:: _static/img/view_cell_0.png
.. |view_cell_1| image:: _static/img/view_cell_1.png
.. |view_cell_2| image:: _static/img/view_cell_2.png
.. |view_cell_3| image:: _static/img/view_cell_3.png
.. |view_cell_4| image:: _static/img/view_cell_4.png
.. |view_cell_5| image:: _static/img/view_cell_5.png
.. |view_cell_6| image:: _static/img/view_cell_6.png
.. |view_cell_7| image:: _static/img/view_cell_7.png
.. |view_cell_10| image:: _static/img/view_cell_10.png


.. |skeletonpoint_connected| image:: _static/img/skeletonpoint_connected.png
.. |skeletonpoint_notconnected| image:: _static/img/skeletonpoint_notconnected.png
.. |skeletonpoint_valide| image:: _static/img/skeletonpoint_valide.png
.. |panel_lateral_machine_notconnected| image:: _static/img/panel_lateral_machine_notconnected_fr.png





